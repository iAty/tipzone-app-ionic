import { Component, ViewChild, OnDestroy } from '@angular/core';
import { Config, Platform, Events, MenuController, Nav, Tabs, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { AuthenticationService } from '../providers/user';
import { UnsplashAPIService } from '../providers/unsplash';
import { TranslateService, LangChangeEvent, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { I18nSwitcherProvider } from '../providers/i18n-switcher';
import { ThemeSwitcherProvider } from '../providers/theme-switcher';
import { environment } from '../environments/environment';
import { PageLoaderProvider } from '../components/page-loader-progress/page-loader.service';
import { LandingPage } from '../pages/landing/landing';

// import { Deeplinks } from '@ionic-native/deeplinks';
import { GroupPage } from '../pages/group/group';
import { LogEntryService } from '../providers/log-events';

export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon?: string;
  pageId?: string;
}


@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnDestroy {
  rootPage: any = null;
  backgroundImages: any;
  pageLoader = true;

  @ViewChild(Nav) nav:Nav;
  @ViewChild('myTabs') tabRef: Tabs;

  private i18nSubscription: Subscription;
  private themeSubscription: Subscription;
  private themeBackgroundSubscription: Subscription;
  private disconnectSubscription: Subscription;
  private connectSubscription: Subscription;
  private pageLoad: Subscription;

  mainTheme = 'default';

  public links = [

  ];

  pages: PageInterface[] = [
    { title: 'DASHBOARD', pageName: 'home', tabComponent: 'home-page', index: 0, icon: 'home' },
    { title: 'TIPS', pageName: 'sport', tabComponent: 'sports-page', index: 1, icon: 'flash' },
    { title: 'GROUPS', pageName: 'group', tabComponent: 'GroupsPage', index: 2, icon: 'people' },
    { title: 'STANDINGS', pageName: 'tablella', tabComponent: 'tabella', index: 3, icon: 'list' },
    // { title: 'STANDINGS', pageName: 'tabs', tabComponent: 'tabella', index: 3, icon: 'list' },
    // { title: 'MY_PROFILE', pageName: 'tabs', tabComponent: 'SettingsPage', index: 4, icon: 'people' }
  ];

  morePages: PageInterface[] = [
    {title: 'IMPRESSUM', pageName: 'static-page', pageId: 'impressum'},
    {title: 'TERMS_AND_CONDITIONS', pageName: 'static-page',  pageId: 'terms_of_use'},
    {title: 'COOKIE_POLICY', pageName: 'static-page',  pageId: 'cookie_policy'},
    {title: 'PRIVACY_AND_POLICY', pageName: 'static-page',  pageId: 'privacy_and_policy'},
    {title: 'FAQ', pageName: 'static-page',  pageId: 'faq'}
  ];


  constructor(
    platform: Platform,
    statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private user: AuthenticationService,
    public menu: MenuController,
    private storage: Storage,
    public app: App,
    private unsplash: UnsplashAPIService,
    public events: Events,
    public translate: TranslateService,
    private i18nSwitcherProvider: I18nSwitcherProvider,
    private themeSwitcherProvider: ThemeSwitcherProvider,
    private pageLoaderProvider: PageLoaderProvider,
    public config: Config,
    // private deeplinks: Deeplinks,
    private logService: LogEntryService,
    private network: Network) {
      this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        // console.log('network was disconnected :-(');
      });
      this.connectSubscription = this.network.onConnect().subscribe(() => {
        // console.log('network has connected :-)');
      });
      this.user.signoutNotification.subscribe( data => {
        // console.log('signoutNotification', data);
      })
      this.user.signinNotification.subscribe( data => {
        if (data) {
        // console.log('notification:',data);
        }
      });


      // this.deeplinks.routeWithNavController(this.nav, {
      //   '/invitation/:user/:code': 'invitation-page'
      // }).subscribe((match) => {
      //   // match.$route - the route we matched, which is the matched entry from the arguments to route()
      //   // match.$args - the args passed in the link
      //   // match.$link - the full link data
      //   // console.log('Successfully matched route', match);
      // }, (nomatch) => {
      //   // nomatch.$link - the full link data
      //   console.error('Got a deeplink that didn\'t match', nomatch);
      // });

      this.translate.setDefaultLang('hu');
      // this.translate.use(this.translate.getBrowserLang());
      // this.translate.use('hu');
      this.i18nSubscription = this.i18nSwitcherProvider.watch().subscribe((lang: string) => {
        this.translate.use(lang);
      });
      this.themeSubscription = this.themeSwitcherProvider.watch().subscribe((theme: string) => {
        this.mainTheme = theme;
      });
      this.themeBackgroundSubscription = this.themeSwitcherProvider.watchBackground().subscribe((backgroundUrl: string) => {
        // console.log('backgroundUrl', backgroundUrl)
        this.backgroundImages = backgroundUrl; 
      });

      this.pageLoad = this.pageLoaderProvider.watch().subscribe((ready: boolean) => {
        this.pageLoader = !ready;
      });

    let globalActions = function () {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (platform.is('cordova')) {
        statusBar.backgroundColorByHexString("#211C5B");
        statusBar.styleLightContent();
        statusBar.overlaysWebView(false);
        splashScreen.hide();
      }
    };

    this.getBackground();
    platform.ready().then(() => {

      user.isAuthenticated()
        .then( 
          iamAuthenticated => {
            console.log('you are authenticated!');
            this.rootPage = 'tabs';
            this.enableMenu(true);
          })
        .catch( 
          notIamNotlogedIn => {
            console.log('you are NOT authenticated!');
        
            this.storage.ready().then( ok => { 
              this.storage.get('user_login_attempt').then( hasAttempt => {
                if (hasAttempt) {
                  this.rootPage = 'login-page';
                  return;
                } else {
                  this.rootPage = 'landing-page';
                  return;
                }
              })
            });
        })
      globalActions();
      

    });
  }

  getBackground() {
    this.unsplash.get(environment.UNSPLASH_COLLECTION_ID).then( backgrounds => {
      this.backgroundImages = backgrounds[this.randomNumber(backgrounds)]
      this.preloadImage(this.backgroundImages);
      })
  }

  preloadImage(img: any) {
    if (img.urls.regular) {
      // console.log('img', img.urls.regular);
      let newImg = new Image();
      newImg.setAttribute('src', img.urls.regular);
      newImg.onload = () => {
        img.loaded = true;
      }
    }
    
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
  }


  /**
   *  @todo: remove static number aftrr release
   * @param list
   */
  randomNumber(list): number {
    // const num = Math.floor(Math.random() * (list.length - 1 + 1));
    return 1;
  }

  // openPage(sport) {
  //   // Reset the nav controller to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   // this.rootPage = page;

  //   // close the menu when clicking a link from the menu
  //   this.menu.close();
  // }

  // listenToLoginEvents() {
  //   this.events.subscribe('user:login', () => {
  //     this.enableMenu(true);
  //   });


  //   this.events.subscribe('user:logout', () => {
  //     this.enableMenu(false);
  //   });
  // }

  // enableMenu(loggedIn) {
  //   this.menu.enable(loggedIn, 'loggedInMenu');
  //   this.menu.enable(!loggedIn, 'loggedOutMenu');
  //   if (!loggedIn) {
  //     this.rootPage = 'login-page';
  //   }
  // }

  ionViewDidEnter() {

  }


  logout(): void {
    this.logService.writeLogEntry('LOGOUT');
    this.user.signout();
    this.rootPage = 'login-page';
    this.menu.close();

  }

  // staticPage(staticPage: string): void {
  //   // this.nav.getRootNav().push(StaticPage, { pageId: staticPage});
  //   this.rootPage = 'StaticPage';
  // }

  openPage(page: PageInterface) {
    let params = {};

    // The index is equal to the order of our tabs inside tabs.ts
    if (page.index) {
      params = { tabIndex: page.index };
    }

    if (page.pageId) {
      params = { pageId: page.pageId };
    }

    // The active child nav is our Tabs Navigation
    // // console.log('active child nav', this.nav.getActiveChildNavs()[0]);
    if (this.nav.getActiveChildNavs()[0] && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } else {
      // Tabs are not active, so reset the root page
      // In this case: moving to or from SpecialPage
      this.nav.setRoot(page.pageName, params);
    }
  }

  isActive(page: PageInterface) {
    // Again the Tabs Navigation
    let childNav = this.nav.getActiveChildNavs()[0];

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    // Fallback needed when there is no active childnav (tabs not active)
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary';
    }
    return;
  }

  setSport(sportId: string): void {
    // // console.log(sportId);
    this.menu.close();
  }

  ngOnDestroy() : void {
    if (this.disconnectSubscription) {
      this.disconnectSubscription.unsubscribe();
    }
    if (this.connectSubscription) {
      this.connectSubscription.unsubscribe();
    }
    if (this.i18nSubscription) {
      this.i18nSubscription.unsubscribe();
    }
    if (this.themeSubscription) {
      this.themeSubscription.unsubscribe();
    }
  }

  restartApp(): void {
    this.logService.writeLogEntry('RESET:APP');
    this.splashScreen.show();
    window.location.reload();
  }
}
