import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
import { environment } from '../environments/environment';
import { appVersion } from './app.version';

if (environment.production) {
  enableProdMode();
}

  platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .then(() => {
      const tpl =
        "background-image: linear-gradient(318deg, #65CCEC, #211C5B 48%, #EF3E2A 88%); font-size:16px; font-weight: 100;padding:3px 5px;color:";
      console.log(
        "%cTip " + "Zone " + "Progressive Web App " + appVersion,
        tpl + "white",
      );
    })
    .catch(err => console.warn(err));
