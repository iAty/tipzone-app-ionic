import { ErrorHandler, Inject, InjectionToken } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import * as Rollbar from 'rollbar';
import { environment } from '../environments/environment';

export const RollbarService = new InjectionToken('rollbar');

const rollbarConfig = {
  accessToken: environment.ROLLBAR_API_KEY,
  captureUncaught: true,
  captureUnhandledRejections: true,
  payload: {
    environment: (environment.production) ? "production" : "development"
  }
};

export function rollbarFactory() {
  return new Rollbar(rollbarConfig);
}

export class RollbarErrorHandler implements ErrorHandler {
  constructor(
    @Inject(AlertController) private alerts: AlertController,
    @Inject(SplashScreen) public splashScreen: SplashScreen,
    @Inject(RollbarService) public rollbar: Rollbar
  ) {}

  async handleError(error) {
    console.warn(error.originalError || error);
    this.rollbar.error(error.originalError || error);
    // this.presentAlert();
  }





  private presentAlert(): void {
    const alert = this.alerts.create({
      title: 'An Error Has Occurred',
      subTitle: 'Unfortunately, the app needs to be restarted',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Restart',
          handler: () => {
            this.splashScreen.show();
            window.location.reload();
          }
        }
      ]
    });
    alert.present();
    }
}
