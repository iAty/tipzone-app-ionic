import { Injectable } from '@angular/core';

declare var AWS: any;
// declare const aws_mobile_analytics_app_id;
declare const aws_cognito_region;
declare const aws_cognito_identity_pool_id;
declare const aws_user_pools_id;
declare const aws_user_pools_web_client_id;
declare const aws_user_files_s3_bucket;
declare const aws_cloud_logic_custom;
declare const aws_user_files_s3_bucket_region;

@Injectable()
export class AwsConfig {
  _defaults: any;
  constructor() {
    this.load();
  }
  public load() {
    let aws_cloud_logic_custom_obj = JSON.parse(JSON.stringify(aws_cloud_logic_custom))
    // Expects global const values defined by aws-config.js
    this._defaults = {
      'region': aws_cognito_region, // region you are deploying (all lower caps, e.g: us-east-1)
      'userPoolId': aws_user_pools_id, // your user pool ID
      'appId': aws_user_pools_web_client_id, // your user pool app ID
      'ClientId': aws_user_pools_web_client_id, // your user pool app ID
      'idpURL': `cognito-idp.${aws_cognito_region}.amazonaws.com`, // cognito idp url
      'identityPool': aws_cognito_identity_pool_id, // your federated identity pool ID
      's3UserBucket': aws_user_files_s3_bucket,
      's3UserRegion': aws_user_files_s3_bucket_region,
      'APIs': aws_cloud_logic_custom_obj.reduce((m, v) => { m[v.name] = v.endpoint; return m }, {})
    }
  }

  public get(key: string): any {
    return this._defaults[key];
  }
}
