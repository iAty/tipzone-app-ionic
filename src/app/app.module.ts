import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';

import { AuthenticationService } from '../providers/user';
import { DynamoDB } from '../providers/aws.dynamodb';
import { UnsplashAPIService } from '../providers/unsplash';
import { DatabaseService } from '../providers/db';

import { NewsFeedService } from '../components/news-feed/news-feed.service';

import { Sigv4Http } from '../providers/sigv4.service';

import { AwsConfig } from './app.config';
import { HttpModule} from '@angular/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { I18nSwitcherProvider } from '../providers/i18n-switcher';

import 'hammerjs';
import { CookieTabComponent } from '../components/cookie-tab/cookie-tab';
import { CookieModule } from 'ngx-cookie';
import { ThemeSwitcherProvider } from '../providers/theme-switcher';
import { environment } from '../environments/environment';
import { Network } from '@ionic-native/network';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider
} from "angularx-social-login";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PopoverPage } from '../components/tip-credit/popover-page';
import { LeaderPopoverPage } from '../components/leader-board/leader-popover-page';

import { RollbarService, rollbarFactory, RollbarErrorHandler } from './tipzone-error-handler.service';
import { PageLoaderProgressComponent } from '../components/page-loader-progress/page-loader-progress';
import { PageLoaderProvider } from '../components/page-loader-progress/page-loader.service';
// import { Deeplinks } from '@ionic-native/deeplinks';
import { LogEntryService } from '../providers/log-events';

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.GOOGLE_CLIENT_ID)
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.FB_APP_ID)
  }
]);

export function provideConfig() {
  return config;
}


@NgModule({
  declarations: [
    MyApp,
    CookieTabComponent,
    PopoverPage,
    PageLoaderProgressComponent,
    LeaderPopoverPage
  ],
  imports: [

    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
       preloadModules: true
    }),
    IonicStorageModule.forRoot({
      name: '_tipzone_config',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    CookieModule.forRoot(),
    SocialLoginModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CookieTabComponent,
    PopoverPage,
    PageLoaderProgressComponent,
    LeaderPopoverPage
  ],
  providers: [
    AwsConfig,
    StatusBar,
    SplashScreen,
    // { provide: ErrorHandler, useClass: (environment.production) ? RollbarErrorHandler : IonicErrorHandler },
    // { provide: RollbarService, useFactory: rollbarFactory },
    Camera,
    AuthenticationService,
    Sigv4Http,
    DynamoDB,
    LogEntryService,
    // Deeplinks,
    NewsFeedService,
    DatabaseService,
    UnsplashAPIService,
    I18nSwitcherProvider,
    ThemeSwitcherProvider,
    PageLoaderProvider,
    Network,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ]
})
export class AppModule {}

declare var AWS;
AWS.config.customUserAgent = AWS.config.customUserAgent + ' Ionic';
