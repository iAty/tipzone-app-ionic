export const environment = {
  production: false,
  FB_APP_ID: '320547508452752',
  // GOOGLE_CLIENT_ID: 'com.googleusercontent.apps.789308346375-j2mhtkbnq276jjm24n074ec85ifeafko',
  GOOGLE_CLIENT_ID: '789308346375-u6ap7efm1euvf4ro2m2c3a1e6e43ri0o.apps.googleusercontent.com',
  S3_URL: '/assets/data/',
  S3_TEMP_URL: '/assets/database/',
  NEWS_FEED_API: '/assets/data/news-feed.json',
  // NEWS_FEED_API: '//newsapi.org/v2/top-headlines',
  NEWS_FEED_API_KEY: '28a6a5e6d810485db5873dc819412c90',
  DATA_API_URL: 'http://example.com/api/',
  UNSPLASH_COLLECTION_ID: 1878811,
  ROLLBAR_API_KEY: 'd1dcc403ba83473bad334d2e0bb54a67',
  LOG_ENTRY_TABLE: 'tipzone-mobilehub-1836564323-LoginTrail'
};
