import { NgModule } from '@angular/core';
import { IterablePipe } from './iterable/iterable.pipe';
import { KeysPipe } from './keys/keys.pipe';
import { SportFilterPipe } from './sport-filter/sport-filter.pipe';

const PIPES = [
  IterablePipe,
  KeysPipe,
  SportFilterPipe
]

@NgModule({
  declarations: PIPES,
  exports: PIPES
})
export class PipesModule {}


