import { PipeTransform, Pipe } from '@angular/core';
/**
 * Sport filter pipe for objects
 * @example
 * <tr *ngFor="let c of content">
 * <td *ngFor="let key of c | selectedsport: 'sport'">{{key}}</td>
 * </tr>
*/
@Pipe({name: 'selectedsport'})
export class SportFilterPipe implements PipeTransform {
  transform(value, arg: string) : any {
    if (!value) { return; }
    if (!arg) { return value; }
    return value.filter( item => item.tournament.sport.name === arg);
  }
}
