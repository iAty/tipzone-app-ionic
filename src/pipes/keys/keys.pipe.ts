import { PipeTransform, Pipe } from '@angular/core';
/**
 * Key/Value pipe for objects
 * @example
 * <tr *ngFor="let c of content">
 * <td *ngFor="let key of c | keys">{{key}}: {{c[key]}}</td>
 * </tr>
*/
@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let keys = [];
    for (let key in value) {
      keys.push(key);
    }
    return keys;
  }
}
