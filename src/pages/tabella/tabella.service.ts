import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { List } from 'immutable';
import * as _orderBy from 'lodash.orderby';

const API_URL = environment.S3_TEMP_URL;
@Injectable()
export class TabellaService {

  _standing: BehaviorSubject<any> = new BehaviorSubject([]);

  constructor(private httpClient: HttpClient) {
    this.getAll()
  }

  get standing () { return Observable.create( fn => this._standing.subscribe(fn) ) }

  getAll(): Observable<any> {
    let observable = this.httpClient.get(API_URL + 'standing.json').share();
    observable.subscribe( (resp: any) => {
      // console.log(resp);
      let data = resp;
      this._standing.next(List(this.sort(data)))
    })
    return observable;
  }

  private sort (standing:any[]): any[] {
    return _orderBy(standing, ['updatedAt', 'createdAt'], ['asc', 'asc'])
  }
}
