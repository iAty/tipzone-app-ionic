import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { TabellaPage } from './tabella';
import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';
import { TabellaService } from './tabella.service';

@NgModule({
  declarations: [
    TabellaPage
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    IonicModule,
    IonicPageModule.forChild(TabellaPage),
  ],
  providers:[
    TabellaService
  ]
})
export class TabellaPageModule {}
