import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { TabellaService } from './tabella.service';
import { DatabaseService } from '../../providers/db';

@IonicPage({
  name: 'tabella',
  segment: 'tablella'
})
@Component({
  selector: 'page-tabella',
  templateUrl: 'tabella.html'
})
export class TabellaPage {
  public tabella: any;
  public items: any;
  public refresher: any;

  constructor(
    public tabellaService: TabellaService,
    private db: DatabaseService) {

  }

  // refreshData(refresher) {
  //   this.refresher = refresher;
  //   this.refreshTabella()
  // }

  doRefresh (refresher) {
    this.db.getStandingBySeasonId('sr:season:41776').then( data => {
      this.tabella = data;
      refresher.complete()
    })
  }


  refreshTabella() {

    // this.db.getDocumentClient().query({
    //   'TableName': this.taskTable,
    //   'IndexName': 'DateSorted',
    //   'KeyConditionExpression': "#userId = :userId",
    //   'ExpressionAttributeNames': {
    //     '#userId': 'userId',
    //   },
    //   'ExpressionAttributeValues': {
    //     ':userId': AWS.config.credentials.identityId
    //   },
    //   'ScanIndexForward': false
    // }).promise().then((data) => {
    //   this.items = data.Items;
    //   if (this.refresher) {
    //     this.refresher.complete();
    //   }
    // }).catch((err) => {
    //   // console.log(err);
    // });
  }

  // generateId() {
  //   var len = 16;
  //   var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  //   var charLength = chars.length;
  //   var result = "";
  //   let randoms = window.crypto.getRandomValues(new Uint32Array(len));
  //   for(var i = 0; i < len; i++) {
  //     result += chars[randoms[i] % charLength];
  //   }
  //   return result.toLowerCase();
  // }

  // addTask() {
  //   let id = this.generateId();
  //   let addModal = this.modalCtrl.create(TabellaCreatePage, { 'id': id });
  //   addModal.onDidDismiss(item => {
  //     if (item) {
  //       item.userId = AWS.config.credentials.identityId;
  //       item.created = (new Date().getTime() / 1000);
  //       this.db.getDocumentClient().put({
  //         'TableName': this.taskTable,
  //         'Item': item,
  //         'ConditionExpression': 'attribute_not_exists(id)'
  //       }, (err, data) => {
  //         if (err) { // console.log(err); }
  //         this.refreshTabella();
  //       });
  //     }
  //   })
  //   addModal.present();
  // }

  // deleteTask(task, index) {
  //   this.db.getDocumentClient().delete({
  //     'TableName': this.taskTable,
  //     'Key': {
  //       'userId': AWS.config.credentials.identityId,
  //       'taskId': task.taskId
  //     }
  //   }).promise().then((data) => {
  //     this.items.splice(index, 1);
  //   }).catch((err) => {
  //     // console.log('there was an error', err);
  //   });
  // }

  ionViewDidEnter(){
    // this.refreshTabella();
    this.db.getStandingBySeasonId('sr:season:41776').then( data => {
      this.tabella = data;
    })
  }

  // get size() { return this.tabellaService.standing.map((standing) => standing.size) }

}
