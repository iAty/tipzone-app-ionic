import { Component } from '@angular/core';
import { App, NavController, NavParams, LoadingController, IonicPage } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService } from '../../providers/user';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PasswordValidation } from '../../providers/password-validation';
import { PageLoaderProvider } from '../../components/page-loader-progress/page-loader.service';

/**
 * Generated class for the InvitationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'invitation-page',
  segment: 'invitation/:user/:code'
})
@Component({
  selector: 'page-invitation',
  templateUrl: 'invitation.html',
})
export class InvitationPage {
  groupName: string = "Baboca Liga";
  userName: string = "Prince";
  seasonName: string = "OTP Bank Liga";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
               public appCtrl: App,
              public user: AuthenticationService,
              public loadingCtrl: LoadingController,
              private fb: FormBuilder,
              private storage: Storage,
              private translate: TranslateService,
              private pageLoaderProvider: PageLoaderProvider) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad InvitationPage ', this.navParams.get('user'), this.navParams.get('code'));
  }

  groupInfo(): void {

  }

  acceptInvitation(): void {
    
  }

  rejectInvitation(): void {
    
  }

  dismiss():void {
    window.location.href="/"
  }
}
