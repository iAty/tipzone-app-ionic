import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/distinct';

const API_URL = environment.S3_URL;
@Injectable()
export class RoundService {
  private rounds: any;
  private oneround: any;
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<any> {
    let isSelected = false;
    if (!this.rounds) {
      this.rounds = this.httpClient.get(API_URL + 'scheduled.187.json')
        .map((res: any) => res.sport_events)
        .mergeMap(items => items)
        .distinct((x: any) => x.tournament_round.number)
        .map((res: any) => {
          let result = {'roundId': res.tournament_round.number, 'scheduled': res.scheduled, 'selected': (res.status === 'not_started' && !isSelected) ? true : false };
          if (!isSelected && res.status === 'not_started') {
            isSelected = true;
          }
          return result;
        })
      return this.rounds;
    } else {
      this.rounds;
    }
  }


  getOneRoundByRoundNumber(roundNumber: string): Observable<any> {

      this.oneround = this.httpClient.get(API_URL + 'scheduled.187.json')
        .map((res: any) => res.sport_events)
        .mergeMap(items => items)
        .filter( (rounds: any) => rounds.tournament_round.number === roundNumber)
      return this.oneround;
  }

}

