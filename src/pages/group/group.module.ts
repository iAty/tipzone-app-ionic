import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { GroupPage } from './group';
import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';
import { RoundService } from './rounds.service';
import { ResultsService } from './results.service';

@NgModule({
  declarations: [
    GroupPage
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    IonicModule,
    IonicPageModule.forChild(GroupPage),
  ],
  providers: [
    RoundService,
    ResultsService
  ]
})
export class GroupPageModule {}
