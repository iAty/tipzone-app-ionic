import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController, IonicPage } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { SlugifyPipe } from 'ngx-pipes';
import { Camera, CameraOptions } from '@ionic-native/camera';

import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/distinctUntilChanged';

import { GroupsStore } from '../../stores/groups.store';
import { AuthenticationService } from '../../providers/user';
import { ResultsStore } from '../../stores/results.store';
import * as _gropuBy from 'lodash.groupby';
import * as _orderBy from 'lodash.orderby';
import { UserStore } from '../../stores/user.store';
import { NewsStore } from '../../stores/news.store';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ThemeSwitcherProvider } from '../../providers/theme-switcher';
import { environment } from '../../environments/environment';
import * as UUID from 'uuid';

declare const aws_user_files_s3_bucket_region;
declare const aws_user_files_s3_bucket;

/**
 * Generated class for the GroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var AWS: any;
@IonicPage({
  segment: 'groups/:groupId/:created'
})
@Component({
  selector: 'page-group',
  templateUrl: 'group.html',
  providers: [
    SlugifyPipe
  ]
})
export class GroupPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild('myInput') myInput: any;
  @ViewChild('avatar') avatarInput;
  @ViewChild('groupbackground') groupbackgroundInput;
  maxLength = 300;
  private s3: any;
  public avatarPhoto: string;
  public selectedPhoto: Blob;
  public roundList: any = [];
  public roundDetailList: any = [];
  public initialSlide = 19;
  public selectedRound: number;
  public isEditing = false;
  public isFavorite = false;
  public isResultLoading = false;
  public isMember: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public isOwner: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isProduction = environment.production;
  public today = new Date().getTime();
  group: any;
  groupId: any;
  hasWelcome: any;
  userName: any;
  selectedSeasonId: any;
  sub: any;
  mode = 'tabella';
  chooseGroup = 'mygroup';
  userDetails = [
    {
      position: 1,
      name: 'David',
      points: 123
    },
    {
      position: 2,
      name: 'Bela',
      points: 344
    },
    {
      position: 3,
      name: 'Jeno',
      points: 44
    },
    {
      position: 4,
      name: 'Pista',
      points: 13
    }
  ]
  groupResults = {};
  selectedGroup: any = null;
  groupDescription = `Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.

  Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

  Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate`;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    private camera: Camera,
    public auth: AuthenticationService,
    private userStore: UserStore,
    public modalCtrl: ModalController,
    public groupsStore: GroupsStore,
    private themeSwitcherProvider: ThemeSwitcherProvider,
    private slugifyPipe: SlugifyPipe,
    public newsStore: NewsStore,
    public resultStore: ResultsStore) {
    this.groupId = navParams.get('groupId');
    this.hasWelcome = navParams.get('created');


    this.group = null;
    this.userName = null;
    this.avatarPhoto = null;
    this.selectedPhoto = null;
    this.s3 = new AWS.S3({
      'params': {
        'Bucket': aws_user_files_s3_bucket
      },
      'region': aws_user_files_s3_bucket_region
    });
    this.sub = AWS.config.credentials.identityId;
  }

  // ionViewCanEnter(): boolean{
  //   // here we can either return true or false
  //   // depending on if we want to leave this view
  //   if(!this.auth.isUserSignedIn()){
  //     setTimeout(() =>{ this.navCtrl.setRoot('login-page')});
  //    }
  //    return this.auth.isUserSignedIn();
  //  }

  ionViewWillEnter() {
    this.selectedRound = null;
    this.isResultLoading = true;
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present();
    
    this.groupsStore.group.subscribe((group) => {
      if (group) {
        this.selectedSeasonId = group[0]['seasonId'];
        this.group = group[0];
        if (group[0]['hasBackground']) { this.refreshAvatar();}
        this.getRoundData();
        if (group[0].featured) {
          this.themeSwitcherProvider.switchColor(this.slugifyPipe.transform(group[0].groupName));
        } else {
          this.themeSwitcherProvider.switchColor('default');
        }
        
        this.newsStore.getGroupNews(this.group.groupId).subscribe( success => {
         // console.log(success);
        }, hasError => console.warn(hasError));
        
        this.isOwner.next(group[0].userId === AWS.config.credentials.identityId);

        this.resultStore.resultsBySeason(group[0]['seasonId']).subscribe( success => {
          this.isResultLoading = false;
          loading.dismiss();
        }, hasError => loading.dismiss());
      }
    }, hasError => loading.dismiss());

    this.resultStore.results.subscribe( (results: any) => {
      let lastIndex = 1;
      if (results) {
      this.groupResults = _gropuBy(_orderBy(results.toArray(), ['scheduled'], ['asc']), 'tournament_round.number');
      for (let key of Object.keys(this.groupResults)) {
        for (let item of this.groupResults[key]) {
          const itemDate = new Date(item.scheduled).getTime();
          const nowDate = new Date().getTime();
          if (itemDate >= nowDate && !this.selectedRound) {
            this.selectedRound = +item.tournament_round.number;
          }

        }
      }

      }
    })

    this.userStore.getConfig().subscribe( userConfig => {
      this.userName = userConfig.username;

       if (userConfig && userConfig.groups) {
         this.isMember.next(userConfig.groups.indexOf(this.groupId) > -1);
       }
    });

    this.groupsStore.selectGroup(this.groupId);

    if (this.hasWelcome === 'welcome') {
      this.userJoinTheGroup();
    }
  }

  resize() {
    this.maxLength = 300 - this.myInput.value.length;
  }

  userJoinTheGroup() {
    this.userStore.getConfig().subscribe( userConfig => {
      if (userConfig && userConfig['groups'] && userConfig['groups'].length && userConfig['groups'].indexOf(this.groupId) === -1) {
        userConfig['groups'].push(this.groupId);
      } else {
        userConfig['groups'] = [];
        userConfig['groups'].push(this.groupId);
      }
      if (userConfig && userConfig['seasons'] && userConfig['seasons'].length && userConfig['seasons'].indexOf(this.selectedSeasonId) === -1) {
        userConfig['seasons'].push(this.selectedSeasonId);
      } else {
        userConfig['seasons'] = [];
        userConfig['seasons'].push(this.selectedSeasonId);
      }

      this.userStore.updateConfig(userConfig)
        .subscribe( saved => {
          this.userStore.refresh();
          // console.log('saved'); this.isMember.next(true);
          },
         hasError => console.warn('hasError', hasError));
    })
  }

  userLeaveGroup() {
    this.userStore.getConfig().subscribe( userConfig => {
      if (userConfig && userConfig['groups']) {
      userConfig['groups'] = userConfig['groups'].filter( x => x !== this.groupId);

      this.userStore.updateConfig(userConfig)
        .subscribe( saved => {
          this.userStore.refresh();
          // console.log('saved'); this.isMember.next(false);
          },
         hasError =>  console.warn('hasError', hasError));
      }
    })
  }

  requestJoinToGroup():void {
    // console.log('requestJoinToGroup');
  }
  // getRoundResultsByMatch(matchId: string): any {
  //   return this.resultsService.getAllByMatchId(matchId).subscribe(result => {
  //     return result.sport_event_status.home_score;
  //     // result.sport_event_status.away_score;
  //   });
  // }




  getRoundData(): void {
    this.roundList = [];

  }

  selectRound(round: any): void {
    this.roundDetailList = [];
    // console.log('round', round);
    // this.resultsService.getAll(round)
    // .subscribe(
    //   (obs) => {
    //     this.roundDetailList = [...this.roundDetailList, obs];
    //   });
  }

  refreshAvatar() {
    this.s3.getSignedUrl('getObject', { 'Key': 'protected/group/' + this.groupId + '/cover.jpg'}, (err, url) => {
      this.avatarPhoto = url;
      this.themeSwitcherProvider.switchBackground({ urls: { regular: url}});
    });
  }


  dataURItoBlob(dataURI) {
    // code adapted from: http://stackoverflow.com/questions/33486352/cant-upload-image-to-aws-s3-from-ionic-camera
    let binary = atob(dataURI.split(',')[1]);
    let array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
  };

    selectAvatar() {
      const options: CameraOptions = {
        quality: 100,
        targetHeight: 200,
        targetWidth: 200,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }

      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.selectedPhoto = this.dataURItoBlob('data:image/jpeg;base64,' + imageData);
        this.upload();
      }, (err) => {
        this.avatarInput.nativeElement.click();
        // Handle error
      });
    }

    uploadFromFile(event) {
      const files = event.target.files;
      var reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = () => {
        this.selectedPhoto = this.dataURItoBlob(reader.result);
        this.upload();
      };
      reader.onerror = (error) => {
        alert('Unable to load file. Please try another.')
      }
    }


    selectBackground() {
      const options: CameraOptions = {
        quality: 100,
        targetHeight: 200,
        targetWidth: 200,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }

      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.selectedPhoto = this.dataURItoBlob('data:image/jpeg;base64,' + imageData);
        this.upload();
      }, (err) => {
        this.groupbackgroundInput.nativeElement.click();
        // Handle error
      });
    }

    uploadBackgroundFromFile(event) {
      const files = event.target.files;
      var reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = () => {
        this.selectedPhoto = this.dataURItoBlob(reader.result);
        this.upload();
      };
      reader.onerror = (error) => {
        alert('Unable to load file. Please try another.')
      }
    }

  upload() {
    let loading = this.loadingCtrl.create({
      content: 'Uploading image...'
    });
    loading.present();

    if (this.selectedPhoto) {
      this.s3.upload({
        'Key': 'protected/group/' + this.groupId + '/avatar.jpg',
        'Body': this.selectedPhoto,
        'ContentType': 'image/jpeg'
      }).promise().then((data) => {
        this.refreshAvatar();
        loading.dismiss();
      }, err => {
        // console.log('upload failed....', err);
        loading.dismiss();
      });
    } else {
      loading.dismiss();
    }
  }

  saveGroupDetails(): void {

  }

  getCompetitorFlag(result: any): string | null {
    if (!result) { return null; }
    let competitorId = result.id.split(":")[2];
    return '/assets/flags/' + competitorId  + '.png';
  }

  nextRound(): void {
    if (this.selectedRound < 33 ) {
      this.selectedRound++;
    }
  }

  prevRound(): void {
    if (this.selectedRound > 1) {
      this.selectedRound--;
    }
  }

  inviteUser():void {
      let newInviteModal = this.modalCtrl.create('NewInvitationPage', { 
        inviteId: UUID(),
        groupName: this.group.groupName,
        userName: this.userName,
        seasonName: this.group.seasonName,
        });
    newInviteModal.onDidDismiss( (inviteData: string) => {
      if (inviteData) {
        // console.log('newInviteModal Done', inviteData);
      };
    });
    newInviteModal.present();
  }

  get size() { return this.groupsStore.groups.map((groups) => groups.size) }
  get resultSize() { return this.resultStore.results.map((results) => results.size) }
}


// var groupBy = function(xs, key) {
//   return xs.reduce(function(rv, x) {
//     (rv[x[key]] = rv[x[key]] || []).push(x);
//     return rv;
//   }, {});
// };




