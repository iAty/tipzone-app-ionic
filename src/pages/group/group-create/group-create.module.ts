import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { GroupCreatePage } from './group-create';
import { ComponentsModule } from '../../../components/components.module';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    GroupCreatePage
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    IonicModule,
    IonicPageModule.forChild(GroupCreatePage),
  ],
  providers: [
  ]
})
export class GroupPageModule {}
