import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Slides, ViewController, IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GroupsStore } from '../../../stores/groups.store';
import { LogEntryService } from '../../../providers/log-events';

/**
 * @todo: 3 steps!
 * @todo: Add name [need to be uniqe name] + add at least one friend or invite one by email, wont be bulic until they accept.
 * @todo: add Type [Public, Private, Closed] and need the description of types
 * @todo: Choose Game  Sport is default in section
 * @todo: Then create
 * @todo: after you can upload the description, image, setup the terms (advance)
 * @todo: share link or invite more friends
 */

declare var AWS: any;

@IonicPage()
@Component({
  selector: 'page-group-create',
  templateUrl: 'group-create.html',
})
export class GroupCreatePage {
  @ViewChild(Slides) slides: Slides;

  group: any = {
    name: '',
    description: ''
  };

  public sportsList: any = [
    { sportId: 'sr:sport:1', sportName: 'Futball'},
    { sportId: 'sr:sport:2', sportName: 'Kézilabda'},
    { sportId: 'sr:sport:4', sportName: 'Hoki'}
  ];

  public gamesList: any = [
    { gameId: 'sr:season:48238', gameName: 'Világbajnokság',  sportId: 'sr:sport:1'},
  ];

  public sub: string = null;
  public friends: any = [
    // { name: 'Ismael	Goodman', friendId: 123, selected: false },
    // { name: 'Samantha	Glover', friendId: 123, selected: true },
    // { name: 'Marian	Banks', friendId: 123, selected: false },
    // { name: 'Rickey	Washington', friendId: 123, selected: false },
    // { name: 'Frankie	Terry', friendId: 123, selected: false }
  ];

  groupForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private groupStore: GroupsStore,
    public viewCtrl: ViewController,
    public fb: FormBuilder,
    private logService: LogEntryService) {



    this.groupForm = fb.group({
      name: ['', Validators.required],
      groupId: this.navParams.get('groupId'),
      emails: [''],
      groupType: ['PUBLIC', Validators.required],
      game: ['', Validators.required],
      sport: {
        sportId: '',
        sportName: ''
      }
    });
  }

  ionViewDidLoad() {
    this.slides.lockSwipes(true); // lock slides
  }

  create(): void {
    if (this.groupForm.valid) {
      this.done();
    }

  }
  nextStep(data?: any): void {
    // console.log(this.slides);
    this.slides.lockSwipes(false);
    this.slides.slideNext(600);
  }

  slideChanged(): void {
    this.slides.lockSwipes(true);
  }

  // get invited() {
  //   return this.groupForm.get('invited');
  // };

  buildInvitedList() {
    const arr = this.friends.map(invite => {
      return this.fb.group({
        id: invite.friendId,
        selected: invite.selected
      });
    });
    return this.fb.array(arr);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  done() {
    const groupDetails = this.groupForm.value;

    if (this.groupForm.invalid) {
      console.warn('Form not valid!');
      return;
    }

    let newGroupData: any = {};
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Creating group, Please wait...'
    });
    loading.present();
    newGroupData.userId = AWS.config.credentials.identityId;
    newGroupData.createdAt = (new Date().getTime() / 1000);
    newGroupData.updatedAt = (new Date().getTime() / 1000);
    newGroupData.featured = false;
    newGroupData.hasCustomStyle = false;
    newGroupData.joinLimit = 10;
    newGroupData.seasonId = groupDetails.game.gameId;
    newGroupData.seasonName = groupDetails.game.gameName;
    newGroupData.groupName = groupDetails.name;
    newGroupData.groupType = groupDetails.groupType;
    newGroupData.groupId = groupDetails.groupId;

    this.groupStore.addGroup(newGroupData).subscribe( success => {
      // console.log('success created');
      this.logService.writeLogEntry('GROUP:CREATED', {groupId: newGroupData.groupId});
      this.viewCtrl.dismiss(newGroupData.groupId);
      loading.dismiss();
    });

  }

}
