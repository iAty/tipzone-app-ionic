import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/reduce';
import 'rxjs/add/operator/concat';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/concatMap';

export interface SportEvent {
  id: string;
  status: string; // closed
  scheduled: string; // date
  tournament_round: {
    type: string;
    number: number;
  };
  home: {
    name: string;
    id: string;
    score?: number;
  };
  away: {
    name: string;
    id: string;
    score?: number;
  }
}

/**
 *
 */
export class TheSportEvent implements SportEvent {
  constructor(public id: string, public status: string, public scheduled: string, public tournament_round: any, public home: any, public away: any) {
  }
}


const API_URL = environment.S3_URL;
@Injectable()
export class ResultsService {
  // private results$: Observable<any>;
  // private roundNumber$: number = null;
  constructor(private httpClient: HttpClient) { }

  getAll(roundNumber: any): Observable<any> {
    // if (!this.results$ && this.roundNumber$ !== roundNumber) {
      return Observable.concat(this.httpClient.get(API_URL + 'results.187.json').map((res: any) => res.results), this.httpClient.get(API_URL + 'scheduled.187.json').map((res: any) => { return res.sport_events; }))
       .mergeMap(items => items)
        .map( (item: any) => {
          return new TheSportEvent(
            (item.sport_event_status) ? item.sport_event.id : item.id,
            (item.sport_event_status) ? item.sport_event_status.status : item.status,
            (item.sport_event_status) ? item.sport_event.scheduled : item.scheduled,
            (item.sport_event_status) ? item.sport_event.tournament_round : item.tournament_round,
            {
              name: (item.sport_event_status) ? item.sport_event.competitors[0].name : item.competitors[0].name,
              id: (item.sport_event_status) ? item.sport_event.competitors[0].id : item.competitors[0].id,
              score: (item.sport_event_status) ? item.sport_event_status.home_score : null
            },
            {
              name: (item.sport_event_status) ? item.sport_event.competitors[1].name : item.competitors[1].name,
              id: (item.sport_event_status) ? item.sport_event.competitors[1].id : item.competitors[1].id,
              score: (item.sport_event_status) ? item.sport_event_status.away_score : null
            }
          );
        })
        .filter( (rounds: any) => {
          return roundNumber === rounds.tournament_round.number;
        })
        .distinct(round => round.id)

        // .reduce( (match, score) => match[score] = score.sport_event_status.home_score)
    // } else {
    //   // console.log('Else roundNumber', roundNumber);
    //   return this.results$;
    // }
  }

  getAllByMatchId(matchId: string): Observable<any> {
    return this.getAll(20).mergeMap(items => items).filter( (item: any) => {
      return item.sport_event.id === matchId;
    })
  }


}
