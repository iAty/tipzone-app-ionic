import { Component } from '@angular/core';
import { Platform, MenuController, NavParams, IonicPage } from 'ionic-angular';
import { PageInterface } from '../../app/app.component';

@IonicPage({
  name: 'tabs',
  segment: 'app'
})
@Component({
  templateUrl: 'tabs.html',
  styles: [`
    .ion-ios-add-circle-outline {
      background: #b8e986;
    }
    ::ng-deep .show-tabbar {
        bottom: 60px;
    }
  `]
})
export class TabsPage {
  home = 'home-page';
  groups = 'GroupsPage';
  sports = 'sports-page';
  task = 'tabella';

  add = 'tabella';
  settings = 'SettingsPage';


  public tabsPlacement: string = 'bottom';
  public tabsLayout: string = 'icon-top';

  myIndex: number;

  public pages: PageInterface[] = [
    { title: 'DASHBOARD', pageName: 'home', tabComponent: 'home-page', index: 0, icon: 'home' },
    { title: 'TIPS', pageName: 'sport', tabComponent: 'sports-page', index: 1, icon: 'flash' },
    { title: 'GROUPS', pageName: 'group', tabComponent: 'GroupsPage', index: 2, icon: 'people' },
    { title: 'STANDINGS', pageName: 'tablella', tabComponent: 'tabella', index: 3, icon: 'list' },
    // { title: 'MY_PROFILE', pageName: 'tabs', tabComponent: 'SettingsPage', index: 4, icon: 'people' }
  ];

  constructor(
    navParams: NavParams,
    public platform: Platform,
    public menu: MenuController) {
    if (!this.platform.is('mobile')) {
      this.tabsPlacement = 'top';
      this.tabsLayout = 'icon-left';
    }
    this.myIndex = 0;
    // // console.log('tabs construtor');
    // Set the active tab based on the passed index from menu.ts
  }

  ionViewDidEnter() {
    
    // // console.log('tabs did enter');
  }

  addGroup(): void {

  }

  openPage() {

  }

  logout() {

  }

}



