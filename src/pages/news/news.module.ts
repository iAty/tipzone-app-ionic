import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsPage } from './news';
import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    NewsPage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    IonicPageModule.forChild(NewsPage),
  ],
})
export class NewsPageModule {}
