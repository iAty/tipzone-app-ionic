import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ViewController } from 'ionic-angular';
import { NewsFeedService } from '../../components/news-feed/news-feed.service';

/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  news: any;
  newsId: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private newsService: NewsFeedService,
    private viewCtrl: ViewController
  ) {
    this.newsService.refresh();
    this.newsId = navParams.get('newsId');
    this.newsService.news.subscribe( newsFeed => {
      this.news = newsFeed.find( item => this.prettifyUrl(item.title) === this.newsId);
    }, error => console.warn(JSON.stringify(error)));
  }

  prettifyUrl(title: string): string {
    return title.toLowerCase().replace(/[^a-zA-Z0-9]/g,'_');
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad NewsPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
