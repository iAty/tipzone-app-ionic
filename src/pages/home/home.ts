import { Component } from '@angular/core';
import { AuthenticationService } from '../../providers/user';
import { IonicPage } from 'ionic-angular';
import { ResultsStore } from '../../stores/results.store';
import { Storage } from '@ionic/storage';
import { NguCarousel } from '@ngu/carousel';
import { GroupsStore } from '../../stores/groups.store';
import { ThemeSwitcherProvider } from '../../providers/theme-switcher';
import { PageLoaderProvider } from '../../components/page-loader-progress/page-loader.service';

@IonicPage({
  name: 'home-page',
  segment: 'home'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  today = new Date();
  isSearch = false;
  results: any;
  selectedSport: string
  isLoading = false;
  public bannerCarousel: NguCarousel;

  constructor(
    private auth: AuthenticationService,
    public storage: Storage,
    public groupsStore: GroupsStore,
    public resultStore: ResultsStore,
    private themeSwitcherProvider: ThemeSwitcherProvider,
    private pageLoaderProvider: PageLoaderProvider
  ) {
    // db.getResults().then( results => this.results = results);
    this.bannerCarousel = {
      grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
      slide: 1,
      speed: 400,
      interval: 11000,
      point: {
        visible: false
      },
      load: 2,
      touch: true,
      loop: true,
      custom: 'banner'
    }

  }

  ionViewWillEnter(){
    // this.themeSwitcherProvider.switchColor('default');
    
  }

  ionViewDidEnter() {
    // console.log('did enter');

    // this.isLoading = true;
    // this.storage.ready().then( isReady => {
    //   this.storage.get('sport').then( selectedSport => {
    //     if (selectedSport && selectedSport !== null) {
    //       // console.log('selectedSport', selectedSport);
    //       this.resultStore.resultsBySport(selectedSport).subscribe( success => this.isLoading = false);
    //       this.selectedSport = selectedSport;
    //     } else {
    //       this.resultStore.resultsAll().subscribe( success => this.isLoading = false);
    //     }

    //   })
    // })

    // this.pageLoaderProvider.isReady(true);
  }

  ionViewDidLoad() {
    // let subscription = this.groupsStore.refresh().subscribe({
    //   complete: () => {
    //     subscription.unsubscribe()
    //   }
    // });
    // console.log('did load');
  }

  // ionViewCanEnter() {
  //   // return this.auth.isUserSignedIn();
  // }

}





