import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';
import { PipesModule } from '../../pipes/pipes.module';
import { NgPipesModule } from 'ngx-pipes';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    PipesModule,
    NgPipesModule,
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule {}
