import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, Refresher } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthenticationService } from '../../providers/user';
import { ResultsStore } from '../../stores/results.store';
import { DatabaseService } from '../../providers/db';
/**
 * Generated class for the SportsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'sports-page',
  priority: 'high'
})
@Component({
  selector: 'page-sports',
  templateUrl: 'sports.html'
})
export class SportsPage {
  sportList = {
    "Futball": {

    }
  };

  sportsFilter = '';
  seasonFilter = '';
  playersFilter = '';
  userPicks: any;
  users: BehaviorSubject<any> = new BehaviorSubject([]);

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthenticationService,
    private db: DatabaseService,
    public resultStore: ResultsStore
  ) {
    // this.resultStore.resultsBySeason();
    
  }

  doRefresh(refresher: Refresher) {
    let subscribtion = this.resultStore.refresh().subscribe({
      complete: () => {
      // subscribtion.unsubscribe();
      refresher.complete();
      }
    });


  }


  ionViewCanEnter(): boolean{
    if(!this.auth.isUserSignedIn()){
      setTimeout(() =>{ this.navCtrl.setRoot('login-page')});
     }
     return this.auth.isUserSignedIn();
   }

  ionViewDidEnter() {
    this.db.getUsersStanding().then(  userResults => {
      // console.log('user results', userResults)
      this.userPicks = userResults;
    });
  }

  navigateToGroups(event: Event, sportId: string): void {
    event.preventDefault();
    this.navCtrl.push('GroupsPage', {
      sportId: sportId
    });
  }

  userPicksByResult(seasonId, resultId): any {
    return (this.userPicks && this.userPicks[seasonId] && this.userPicks[seasonId][resultId]) ? this.userPicks[seasonId][resultId] : null;
  }

  get size() { return this.resultStore.results.map((results) => results.length || 0) }
}



