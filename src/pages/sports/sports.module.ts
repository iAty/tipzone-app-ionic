import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SportsPage } from './sports';
import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    SportsPage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    IonicPageModule.forChild(SportsPage),
  ],
})
export class SportsPageModule {}
