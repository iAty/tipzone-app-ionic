import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { PageLoaderProvider } from '../../components/page-loader-progress/page-loader.service';
/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'landing-page',
  segment: 'landing'
})
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {
  pageContent: any;
  subscribe: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private httpClient: HttpClient,
    private translate: TranslateService,
    private pageLoaderProvider: PageLoaderProvider) {
  }

  ngOnInit() {
      this.subscribe = this.getMarkdownByPageId('landing').subscribe(
        pageContent => (this.pageContent = pageContent)
      );
  }

  ionViewDidEnter() {
    this.pageLoaderProvider.isReady(true);
  }

  getMarkdownByPageId(pageId: string): Observable<any> {
    const currentLang = this.translate.currentLang || 'en';
    const pagePath = ['/assets/markdown/', currentLang, '/', pageId, '.md'].join('');

    return this.httpClient.get(pagePath, { responseType: "text" });
  }

  ngOnDestroy() {
    if (this.subscribe) {
      // this.subscribe.unsubscribe();
    }
  }
}

