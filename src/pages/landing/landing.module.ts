import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandingPage } from './landing';
import { SharedModule } from '../../shared/shared.module';
import { ComponentsModule } from '../../components/components.module';
import { TranslateService } from '@ngx-translate/core';

@NgModule({
  declarations: [
    LandingPage,
  ],
  imports: [
    SharedModule,
    ComponentsModule,
    IonicPageModule.forChild(LandingPage),
  ]
})
export class LandingPageModule {}
