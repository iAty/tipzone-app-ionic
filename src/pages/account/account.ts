import { Component, ViewChild } from '@angular/core';

import { LoadingController, NavController, IonicPage } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { DynamoDB } from '../../providers/providers';
import { AuthenticationService } from '../../providers/user';
import { AwsConfig } from '../../app/app.config';
import { I18nSwitcherProvider } from '../../providers/i18n-switcher';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { appVersion, lastAppUpdate } from '../../app/app.version';
import { BehaviorSubject } from 'rxjs';
import { LogEntryService, Stuff } from '../../providers/log-events';


declare var AWS: any;
@IonicPage({
  name: 'account-page',
  segment: 'account'
})
@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {

  @ViewChild('avatar') avatarInput;

  private s3: any;
  public avatarPhoto: string;
  public selectedPhoto: Blob;
  public attributes: any;
  public sub: string = null;
  lang = 'hu';
  version = appVersion;
  lastUpdate = lastAppUpdate;
  public logdata: Array<Stuff> = [];

  public userDetailForm: FormGroup;
  public userNotificationForm: FormGroup;


  constructor(public navCtrl: NavController,
              public user: AuthenticationService,
              public fb: FormBuilder,
              public db: DynamoDB,
              private awsConf: AwsConfig,
              public camera: Camera,
              public i18nSwitcherProvider: I18nSwitcherProvider,
              public loadingCtrl: LoadingController,
            private logService: LogEntryService) {
    this.attributes = [];
    this.avatarPhoto = null;
    this.selectedPhoto = null;
    this.s3 = new AWS.S3({
      'params': {
        'Bucket': this.awsConf.get('s3UserBucket')
      },
      'region': this.awsConf.get('s3UserRegion')
    });
    this.sub = AWS.config.credentials.identityId;

    this.refreshAvatar();
    this.userDetailForm = this.fb.group({
      username: this.user.cognitoUser['username'],
      language: this.lang,
      country: [''],
      userId: AWS.config.credentials.identityId
    });

    this.logService.getLogEntries(this.logdata);

    this.userNotificationForm = this.fb.group({
      newsletter: [false, [Validators.requiredTrue]],
      emailNotification: [false, [Validators.requiredTrue]],
      pushNotification: [false, [Validators.requiredTrue]]
    })


    // this.user.getConfig().subscribe( userData => {
    //   if (userData && userData.username && userData.language && userData.country) {
    //     this.userDetailForm.patchValue({
    //       username: userData.username,
    //       language: userData.language,
    //       country: userData.country
    //     });

    //     this.userNotificationForm.patchValue({
    //       newsletter: userData.newsletter,
    //       emailNotification: userData.emailNotification,
    //       pushNotification: userData.pushNotification
    //     });
    //   }
    // })

    this.userDetailForm.valueChanges.subscribe( newValue => {
      newValue.credit = 2345;
      // this.user.setConfig();
      // this.storage.ready().then( ready => this.storage.set('user_config', data));
      this.logService.writeLogEntry('USER:CHANGE');
      this.i18nSwitcherProvider.switchLang(newValue.language);
      // console.log(newValue.language);
    })

    this.userNotificationForm.valueChanges.subscribe( newNotificationValue => {
      // this.user.setConfig(newNotificationValue);
      this.logService.writeLogEntry('NOTIFICATIONS:CHANGE');
    })
  }

  refreshAvatar() {
    this.user.refreshAvatar().then( url => this.avatarPhoto = url);
  }



  dataURItoBlob(dataURI) {
    // code adapted from: http://stackoverflow.com/questions/33486352/cant-upload-image-to-aws-s3-from-ionic-camera
    let binary = atob(dataURI.split(',')[1]);
    let array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
  };

  selectAvatar() {
    const options: CameraOptions = {
      quality: 100,
      targetHeight: 200,
      targetWidth: 200,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.selectedPhoto  = this.dataURItoBlob('data:image/jpeg;base64,' + imageData);
      this.upload();
    }, (err) => {
      this.avatarInput.nativeElement.click();
      // Handle error
    });
  }



  uploadFromFile(event) {
    const files = event.target.files;
    // console.log('Uploading', files)
    var reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = () => {
      this.selectedPhoto = this.dataURItoBlob(reader.result);
      this.upload();
    };
    reader.onerror = (error) => {
      alert('Unable to load file. Please try another.')
    }
  }

  upload() {
    let loading = this.loadingCtrl.create({
      content: 'Uploading image...'
    });
    loading.present();

    if (this.selectedPhoto && this.sub) {
      this.s3.upload({
        'Key': 'public/user/' + this.sub + '/avatar.jpg',
        'Body': this.selectedPhoto,
        'ContentType': 'image/jpeg'
      }).promise().then((data) => {
        this.refreshAvatar();
        // console.log('upload complete:', data);
        loading.dismiss();
      }, err => {
        // console.log('upload failed....', err);
        loading.dismiss();
      });
    } else {
      loading.dismiss();
    }
  }

  logout() {
    this.user.signout();
    this.navCtrl.setRoot('login-page');
  }

  dismiss():void {

  }

}
