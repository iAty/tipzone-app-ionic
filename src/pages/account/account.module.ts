import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountPage } from './account';
import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';
import { I18nSwitcherProvider } from '../../providers/i18n-switcher';

@NgModule({
  declarations: [
    AccountPage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    IonicPageModule.forChild(AccountPage),
  ],
  providers: [
    I18nSwitcherProvider
  ]
})
export class AccountPageModule {}
