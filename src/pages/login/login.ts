import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage } from 'ionic-angular';


import { AuthenticationService, GenericUserDetails } from '../../providers/user';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PasswordValidation } from '../../providers/password-validation';
import { PageLoaderProvider } from '../../components/page-loader-progress/page-loader.service';


@IonicPage({
  name: 'login-page',
  segment: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public mode = 'login';
  error: any;
  errorSignUp: any;
  public loginDetails: any = {
    username: null
  }

  constructor(public navCtrl: NavController,
              public user: AuthenticationService,
              public loadingCtrl: LoadingController,
              private fb: FormBuilder,
              private storage: Storage,
              private pageLoaderProvider: PageLoaderProvider
            ) { }

  ionViewDidEnter(){
    // this.pageLoaderProvider.isReady(true);
  }

  resetPassword():void {
    if (!this.loginDetails || !this.loginDetails.username) {
      return;
    }
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.user.resetUserPassword(this.loginDetails.username).then( success => {
      // console.log('success', success);
      loading.dismiss();

    }).catch( hasError => {
      loading.dismiss();
      console.error(hasError)
    });
  }

  loginFacebook():void {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.user.loginFacebook().then( success => {
      console.log('loginFacebook', success);
      
      const data = new GenericUserDetails(success);

      this.storage.set('user_config', data)
      loading.dismiss();
      this.user.goToLogin().then( navigateTo => {
        if (navigateTo) {
          this.navCtrl.setRoot(navigateTo);
        }
      }).catch( hasError => console.error(hasError));
    })
  }

  loginGoogle(): void {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.user.loginGoogle()
      .then(res => {
        // console.log('success google login', res);
        loading.dismiss();
        this.user.goToLogin().then( navigateTo => {
          if (navigateTo) {
            this.navCtrl.setRoot(navigateTo);
          }
        }).catch( hasError => console.error(hasError));
      })
      .catch(err => {
        // console.log('Error logging into Google', err);
        loading.dismiss();
      });
  }

}


