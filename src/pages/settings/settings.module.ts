import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsPage } from './settings';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    SettingsPage
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(SettingsPage),
  ],
})
export class SettingsPageModule {}
