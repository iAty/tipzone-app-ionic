import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';

import { AuthenticationService } from '../../providers/user';

@IonicPage()
@Component({
  templateUrl: 'settings.html'
})
export class SettingsPage {

  public aboutPage = 'AboutPage';
  public accountPage = 'AccountPage';

  constructor(public user: AuthenticationService, public navCtrl: NavController) {
  }

  logout() {
    this.user.signout();
    this.navCtrl.setRoot('login-page');
  }

}
