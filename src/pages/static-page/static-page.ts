import { Component, OnInit } from "@angular/core";

import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import { NavParams, IonicPage } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@IonicPage({
  name: 'static-page',
  segment: 'static/:pageId/:pageSection'
})
@Component({
  templateUrl: 'static-page.html'
})

export class StaticPage implements OnInit {
  subscribe: any;
  pageContent: any;
  pageId = null

  constructor(
    public navParams: NavParams,
    public translate: TranslateService,
    private httpClient: HttpClient) {
      this.pageId = this.navParams.get('pageId');
      // console.log('has section', this.navParams, this.navParams.get('pageSection'))
   }

  ngOnInit() {
    if (this.pageId) {
      this.getMarkdownByPageId(this.pageId).subscribe(
        pageContent => (this.pageContent = pageContent)
      );
    }
  }

  getMarkdownByPageId(pageId: string): Observable<any> {
    const currentLang = (this.translate && this.translate.currentLang) ? this.translate.currentLang : 'hu';
    const pagePath = "/assets/markdown/" + currentLang + "/" + pageId + ".md";
    return this.httpClient.get(pagePath, { responseType: "text" });
  }

  ngOnDestroy() {
    if (this.subscribe) {
      // this.subscribe.unsubscribe();
    }
  }
}
