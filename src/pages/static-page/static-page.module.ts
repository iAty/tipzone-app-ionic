import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StaticPage } from './static-page';
import { ComponentsModule } from '../../components/components.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    StaticPage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    IonicPageModule.forChild(StaticPage),
  ],
  entryComponents: [
    StaticPage
  ]
})
export class StaticPageModule {}
