import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { GroupsPage } from './groups';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    GroupsPage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    PipesModule,
    IonicModule,
    IonicPageModule.forChild(GroupsPage),
  ],
})
export class GroupsPageModule {}
