import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ModalController } from 'ionic-angular';
import { AuthenticationService } from '../../providers/user';
import { DynamoDB } from '../../providers/providers';
import { GroupsStore } from '../../stores/groups.store';
import { UserStore } from '../../stores/user.store';
import { Storage } from '@ionic/storage';
import * as UUID from 'uuid';
import { DatabaseService } from '../../providers/db';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
declare var AWS: any;
/**
 * Generated class for the GroupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage({
  segment: 'groups'
})
@Component({
  selector: 'page-groups',
  templateUrl: 'groups.html',
})
export class GroupsPage {

  public groupsFiltered: any;
  chooseGroup = 'mygroup';
  groups: any = []
  isSearching: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  icons = 'csoportjaim';
  sportList = [
    {
      title: 'Football',
      sportId: 'sr:sport:1',
      groups: 28,
      image: '/assets/imgs/sports/football-sport.png',
      link: '/'
    },
    {
      title: 'Handball',
      sportId: 'sr:sport:2',
      groups: 13,
      image: '/assets/imgs/sports/handball-sport.png',
      link: '/'
    },
    {
      title: 'Ice Hockey',
      sportId: 'sr:sport:4',
      groups: 2,
      image: '/assets/imgs/sports/hockey-sport.png',
      link: '/'
    }
  ];
  sportListFiltered: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public user: AuthenticationService,
    public storage: Storage,
    public userStore: UserStore,
    public modalCtrl: ModalController,
    public groupsStore: GroupsStore,
    public dataService: DatabaseService,
    public db: DynamoDB) {
      if (navParams.get('sportId')) {
        // console.log('selected sport', navParams.get('sportId'));
        this.storage.set('sport', navParams.get('sportId'));
      }

      this.userStore.getConfig().subscribe( userConfig => {
        // userConfig
      })
  }

  ionViewCanEnter(): boolean{
    if(!this.user.isUserSignedIn()){
      setTimeout(() =>{ this.navCtrl.setRoot('login-page')});
     }
     return this.user.isUserSignedIn();
   }

   ionViewDidEnter(){
      this.doRefresh();
      this.sportListFiltered = this.sportList;
   }

  doRefresh () {
    let subscription = this.groupsStore.refresh().subscribe({
      complete: () => {
        // subscription.unsubscribe()
      }
    });
    let usersub = this.groupsStore.userGroups().subscribe({
      complete: () => {
        // usersub.unsubscribe()
      }
    });
  }

  selectItem(item: any): void {
    if (!item.closed) {
        this.navCtrl.push('GroupPage', {
          groupId: item.groupId
        });
    }
    this.groups.map((item: any) => item.active = false);
    item.active = true;
  }

  refreshData() {
    // this.refresher = refresher;
    this.refreshTasks()
  }

  refreshTasks() {
    // this.dataService.getAllGroups()
    //     .then(
    //       groupsList => this.groupsFiltered = this.groupBy(groupsList.Items, group => group.gameId)
    //     )
    //     .catch(
    //       hasError => console.warn(hasError)
    //     );
    //     this.dataService.getGroupsByUser()
    //       .then(
    //         myGroups => this.groups = myGroups
    //       );
  }

  saveGroupDetails(): void {


  }

  getItems(searchString: any): void {
    // if (searchString.target.value.length < 3) {
    //   return;
    // }
    // console.log('searchString', searchString);
    this.isSearching.next(true);
    this.sportListFiltered = this.sportList.filter(
        item => (searchString.target.value) ? item.title.toLowerCase().includes(searchString.target.value.toLowerCase()) : true)


  }

  getGroupNameById(gameId: string): string {
    let output ='';
    switch (gameId) {
      case 'G001':
        output= 'OTP Bank Liga';
      break;
      case 'G002':
      output= 'Premier League';
      break;
      case 'G003':
      output= 'Champions League';
      break;
      case 'G004':
      output= 'La Liga';
      break;
      case 'G005':
      output= 'Champions League';
      break;
      case 'G006':
      output= 'Bundesliga';
      break;
      case 'G007':
      output= 'Bajnokok Ligája';
      break;
      case 'G008':
      output= 'Merkantil Bank Liga';
      break;
      default:
      output= 'Series A';
      break;
    }
    return output;
  }

  groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
  }

  createGroup(): void {
    let newGroupModal = this.modalCtrl.create('GroupCreatePage', { groupId: UUID()  });
    newGroupModal.onDidDismiss( (groupData: string) => {
      if (groupData) {
        // console.log('Group Done', groupData);
          this.navCtrl.setRoot('GroupPage', {
            groupId: groupData,
            created: 'welcome'
          });
      };
    });
    newGroupModal.present();
  }

  refreshSearch(): void {
    this.isSearching.next(false);
  }

  get size() { return this.groupsStore.groups.map((groups) => groups.size) }
  get usergroupsize() { return this.groupsStore.usergroups.map((usergroups) => usergroups.size) }
}

