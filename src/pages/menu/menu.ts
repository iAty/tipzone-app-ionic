import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Nav, App } from 'ionic-angular';


@IonicPage({
  name: 'menu'
})
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  // Basic root for our content view
  rootPage = 'tabs';

  // Reference to the app's root nav
  @ViewChild(Nav) nav: Nav;



  constructor(public navCtrl: NavController, public appCtrl: App) { }



  logout() {

  }

  staticPage(pageId: string): void {
    this.appCtrl.getRootNav()('static-page', { 'pageId': pageId });
  }

}
