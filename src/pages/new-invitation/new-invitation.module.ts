import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewInvitationPage } from './new-invitation';

@NgModule({
  declarations: [
    NewInvitationPage,
  ],
  imports: [
    IonicPageModule.forChild(NewInvitationPage),
  ],
})
export class NewInvitationPageModule {}
