import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { Sigv4Http } from '../../providers/sigv4.service';
import { AuthenticationService } from '../../providers/user';
import { Observable } from 'rxjs/Observable';
import { AwsConfig } from '../../app/app.config';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import 'rxjs/add/operator/share'

/**
 * Generated class for the NewInvitationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-invitation',
  templateUrl: 'new-invitation.html',
})
export class NewInvitationPage {

  /**
    * @name form
    * @type {FormGroup}
    * @public
    * @description     Defines a FormGroup object for managing the template form
    */
   public form 			: FormGroup;
   private endpoint:string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthenticationService,
    private sigv4: Sigv4Http,
    private config: AwsConfig,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    private _FB          : FormBuilder) {
      this.endpoint = this.config.get('APIs')['invitation'];
      this.form = this._FB.group({
         inviteId : navParams.get('inviteId'),
         groupName: navParams.get('groupName'),
         userName: navParams.get('userName'),
         seasonName: navParams.get('seasonName'),
         people     : this._FB.array([
            this.initTechnologyFields()
         ])
      });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad NewInvitationPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  /**
    * Generates a FormGroup object with input field validation rules for
    * the technologies form object
    *
    * @public
    * @method initTechnologyFields
    * @return {FormGroup}
    */
   initTechnologyFields() : FormGroup
   {
      return this._FB.group({
         email 		: ['', Validators.email]
      });
   }



   /**
    * Programmatically generates a new technology input field
    *
    * @public
    * @method addNewInputField
    * @return {none}
    */
   addNewInputField() : void
   {
      const control = <FormArray>this.form.controls.people;
      control.push(this.initTechnologyFields());
   }



   /**
    * Programmatically removes a recently generated technology input field
    *
    * @public
    * @method removeInputField
    * @param i    {number}      The position of the object in the array that needs to removed
    * @return {none}
    */
   removeInputField(i : number) : void
   {
      const control = <FormArray>this.form.controls.people;
      control.removeAt(i);
   }



   /**
    * Receive the submitted form data
    *
    * @public
    * @method manage
    * @param val    {object}      The posted form data
    * @return {none}
    */
   manage(val : any) : void
   {
     let loading = this.loadingCtrl.create({
      content: 'Sending invitations...'
    });
    loading.present();
      this.auth.getCredentials().map(creds => this.sigv4.post(this.endpoint, 'send-invitation', val, creds)).concatAll().share().subscribe( success => {
        loading.dismiss();
        this.viewCtrl.dismiss();
      }, hasError => {
        console.warn('Invitation has error', hasError);
        loading.dismiss();
        this.viewCtrl.dismiss();
      });
   }
}


