import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { AboutPage } from './about';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    AboutPage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    PipesModule,
    IonicModule,
    IonicPageModule.forChild(AboutPage),
  ],
})
export class AboutPageModule {}
