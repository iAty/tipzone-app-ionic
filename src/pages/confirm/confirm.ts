import { Component } from '@angular/core';

import { NavController, NavParams, IonicPage } from 'ionic-angular';


import { AuthenticationService } from '../../providers/user';
@IonicPage()
@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html'
})
export class ConfirmPage {

  public code: string;
  public username: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public user: AuthenticationService) {
    this.username = navParams.get('username');
  }

  confirm() {
    this.user.confirm({username: this.username, confcode: this.code}).then(() => {
      this.navCtrl.setRoot('login-page');
    });
  }

  resendCode(event: Event): void {
    event.preventDefault();
    this.user.resendRegistrationCode(this.username)
      .then( hasSuccess => console.info('resent confirmation'));
  }


}
