import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { ConfirmPage } from './confirm';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    ConfirmPage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    PipesModule,
    IonicModule,
    IonicPageModule.forChild(ConfirmPage),
  ],
})
export class ConfirmPageModule {}
