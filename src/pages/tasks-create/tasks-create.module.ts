import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { TasksCreatePage } from './tasks-create';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    TasksCreatePage,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    PipesModule,
    IonicModule,
    IonicPageModule.forChild(TasksCreatePage),
  ],
})
export class TasksCreatePageModule {}
