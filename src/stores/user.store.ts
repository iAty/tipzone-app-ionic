import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import 'rxjs/add/operator/share'

import { Sigv4Http } from '../providers/sigv4.service';
import { AuthenticationService } from '../providers/user';
import { AwsConfig } from '../app/app.config';
import { Storage } from '@ionic/storage';

let userstoreFactory = (sigv4: Sigv4Http, auth: AuthenticationService, config: AwsConfig, storage: Storage) => { return new UserStore(sigv4, auth, config, storage) }

const displayFormat = 'YYYY-MM-DD'
declare var AWS: any;

@Injectable()
export class UserStore {
  private _userConfig: BehaviorSubject<any> = new BehaviorSubject({});
  private endpoint:string;

   constructor (
     private sigv4: Sigv4Http, 
     private auth: AuthenticationService, 
     private config: AwsConfig,
     private storage: Storage) {
     this.endpoint = this.config.get('APIs')['test user config'];
     this.refresh();
   }

    get userConfig () { return Observable.create( fn => this._userConfig.subscribe(fn) ) }

    refresh () : Observable<any> {
      if (this.auth.isUserSignedIn()) {
      let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'get-user-config', creds)).concatAll().share();
      observable.subscribe(resp => {
        let data = resp.json();
        this.storage.ready().then( ready => this.storage.set('user_config', data));
        this._userConfig.next(data);
      })
      return observable;
    } else {
      this._userConfig.next({})
      return Observable.of({});
    }
  }

  getConfig (): Observable<any> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'get-user-config', creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let data = resp.json()
        this._userConfig.next(data);
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  updateConfig (newConfig): Observable<any> {
    return this.auth.getCredentials().map(creds => this.sigv4.post(this.endpoint, 'get-user-config', newConfig, creds)).concatAll().share();
  }


}


export let UserStoreProvider = {
  provide: UserStore,
  useFactory: userstoreFactory,
  deps: [Sigv4Http, AuthenticationService, AwsConfig, Storage]
}
