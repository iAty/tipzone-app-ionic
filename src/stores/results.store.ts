
import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import 'rxjs/add/operator/share'
import { List } from 'immutable'
import * as moment from 'moment'
import * as _orderBy from 'lodash.orderby'


import { Sigv4Http } from '../providers/sigv4.service';
import { AuthenticationService } from '../providers/user';
import { Result } from '../models/results.model';
import { AwsConfig } from '../app/app.config';


let resultstoreFactory = (sigv4: Sigv4Http, auth: AuthenticationService, config: AwsConfig) => { return new ResultsStore(sigv4, auth, config) }

const displayFormat = 'YYYY-MM-DD'
declare var AWS: any;


@Injectable()
export class ResultsStore {
  private _results: BehaviorSubject<Array<Result>> = new BehaviorSubject([]);
  private _filters: BehaviorSubject<any> = new BehaviorSubject({});
  private _sports: BehaviorSubject<List<Result>> = new BehaviorSubject(List([]));
  private _seasons: BehaviorSubject<List<Result>> = new BehaviorSubject(List([]));
  private _group: BehaviorSubject<Result> = new BehaviorSubject(null);
  private endpoint:string;
  private userEndpoint:string;


  constructor (private sigv4: Sigv4Http, private auth: AuthenticationService, private config: AwsConfig) {
    this.endpoint = this.config.get('APIs')['resultsCRUD']
    this.userEndpoint = this.config.get('APIs')['user-resultsCRUD']
    // this.endpoint = '/assets/database';
    // this.auth.signoutNotification.subscribe(() => this._results.next([]))
    // this.auth.signinNotification.subscribe(() => this.refresh() )
    this.refresh()
  }

  get results () { return Observable.create( fn => this._results.subscribe(fn) ) }
  get filters () { return Observable.create( fn => this._filters.subscribe(fn) ) }
  get sports () { return Observable.create( fn => this._sports.subscribe(fn) ) }
  get seasons () { return Observable.create( fn => this._seasons.subscribe(fn) ) }

  refresh () : Observable<any> {
    if (this.auth.isUserSignedIn()) {
      let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'results', creds)).concatAll().share()
      observable.subscribe(resp => {
        let data = resp.json();
        let filters = {
          'all': []
        };
        let seasonRounds = {};
        data.reduce(function (acc, obj) {
          var sport = obj['tournament']['sport']['name'];
          var round = obj['tournament_round']['number'];

          seasonRounds[obj['season']['id']] = (seasonRounds[obj['season']['id']]) ? [...seasonRounds[obj['season']['id']], round] : [round];

          var season = {id: obj['season']['id'], name: obj['season']['name'], rounds: seasonRounds[obj['season']['id']].reduce((x, y) => x.includes(y) ? x : [...x, y], []).sort((a, b) => a - b)};

          filters[sport] = [];
          filters[sport].push(season);
          filters['all'].push(season);

          filters[sport] = filters[sport].reduce((x, y) => x.includes(y) ? x : [...x, y], []);
          filters['all'] = filters[sport].reduce((x, y) => x.includes(y) ? x : [...x, y], []);
        //   sports.push(sport);
        //   // // console.log(sport, {id: obj['season']['id'], name: obj['season']['name']})
        //   // // console.log(seasons[sport]=1);
        //   if (sport && season) {
        //     // seasons[sport].push({id: obj['season']['id'], name: obj['season']['name']});
        //   }

          return acc;
        }, {});
        // sports = sports.reduce((x, y) => x.includes(y) ? x : [...x, y], [])
        // for (let eachSport in seasons) {
        //   seasons[eachSport] = seasons[eachSport].reduce((x, y) => x.includes(y) ? x : [...x, y], [])
        // }
        // this._sports.next(List(sports));
        // this._seasons.next(List(seasons));
        this._filters.next(filters);
        this._results.next(this.sort(resp.json()))
      })
      return observable
    } else {
      this._results.next([])
      return Observable.from([])
    }
  }

  resultsAll (): Observable<Result> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'results/sports', creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let data = resp.json()
        this._results.next(this.sort(data))
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  userStanding (): Observable<Result> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.userEndpoint, 'user-results/standing', creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let data = resp.json()
        // console.log('userStanding', data);
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  resultsBySport (sportId): Observable<Result> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.post(this.endpoint, 'results/sports', {sportId: sportId}, creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let data = resp.json()
        this._results.next(this.sort(data))
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  resultsBySeason (seasonId): Observable<Result> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.post(this.endpoint, 'results/season', {seasonId: seasonId}, creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let data = resp.json()
        this._results.next(this.sort(data))
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  resultsByUser (): Observable<Result> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'results/user', creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let data = resp.json()
        this._results.next(this.sort(data))
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  selectResult (resultId): Observable<Result> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.post(this.endpoint, 'results', resultId, creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let group = resp.json()
        this._group.next(group);
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  addResult (group): Observable<Result> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.post(this.endpoint, 'results', group, creds)).concatAll().share()

    observable.subscribe(resp => {
      if (resp.status === 200) {
        let results = [];
        let group = resp.json().group
        results.push(group)
        this._results.next(this.sort(results))
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json().group : null)
  }

  deleteResult (index): Observable<Result> {
    let results = this._results.getValue();
    let obs = this.auth.getCredentials().map(creds => this.sigv4.del(this.endpoint, `results/${results[index].resultId}`, creds)).concatAll().share()

    obs.subscribe(resp => {
      if (resp.status === 200) {
        results.splice(index, 1)[0]
        this._results.next(<Result[]>results)
      }
    })
    return obs.map(resp => resp.status === 200 ? resp.json().group : null)
  }

  completeResult (index): Observable<Result> {
    let results = [];
    let obs = this.auth.getCredentials().map(creds => this.sigv4.put(
      this.endpoint,
      `results/${results[index].resultId}`,
      {completed: true, completedOn: moment().format(displayFormat)},
      creds)).concatAll().share()

    obs.subscribe(resp => {
      if (resp.status === 200) {
        results[index] = resp.json().group
        this._results.next(this.sort(results))
      }
    })

    return obs.map(resp => resp.status === 200 ? resp.json().group : null)
  }


  private sort (results:Result[]): Result[] {
    return _orderBy(results, ['scheduled'], ['asc'])
  }
}

export let ResultsStoreProvider = {
  provide: ResultsStore,
  useFactory: resultstoreFactory,
  deps: [Sigv4Http, AuthenticationService, AwsConfig]
}
