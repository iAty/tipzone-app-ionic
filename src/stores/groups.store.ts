
import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import 'rxjs/add/operator/share'
import { List } from 'immutable'
import * as moment from 'moment'
import * as _orderBy from 'lodash.orderby'


import { Sigv4Http } from '../providers/sigv4.service';
import { AuthenticationService } from '../providers/user';
import { Group } from '../models/groups.model';
import { AwsConfig } from '../app/app.config';


let groupstoreFactory = (sigv4: Sigv4Http, auth: AuthenticationService, config: AwsConfig) => { return new GroupsStore(sigv4, auth, config) }

const displayFormat = 'YYYY-MM-DD'
declare var AWS: any;

@Injectable()
export class GroupsStore {
  private _groups: BehaviorSubject<Array<Group>> = new BehaviorSubject([])
  private _featured: BehaviorSubject<Array<Group>> = new BehaviorSubject([])
  private _userGroups: BehaviorSubject<Array<Group>> = new BehaviorSubject([])
  private _group: BehaviorSubject<Group> = new BehaviorSubject(null);
  private endpoint:string


  constructor (private sigv4: Sigv4Http, private auth: AuthenticationService, private config: AwsConfig) {
    this.endpoint = this.config.get('APIs')['sport-groupsCRUD']
    // this.endpoint = '/assets/database';
    // this.auth.signoutNotification.subscribe(() => this._groups.next([]))
    // this.auth.signinNotification.subscribe(() => this.refresh() )
    this.refresh()
  }

  get groups () { return Observable.create( fn => this._groups.subscribe(fn) ) }
  get featured () { return Observable.create( fn => this._featured.subscribe(fn) ) }
  get usergroups () { return Observable.create( fn => this._userGroups.subscribe(fn) ) }
  get group () { return Observable.create( fn => this._group.subscribe(fn) ) }

  refresh () : Observable<any> {
    if (this.auth.isUserSignedIn()) {
      let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'sport-groups', creds)).concatAll().share()
      observable.subscribe(resp => {
        let data = resp.json()
        this._groups.next(this.sort(data))
        this._featured.next(this.filter(data, 'featured', true));

      })
      return observable
    } else {
      this._groups.next([])
      return Observable.from([])
    }
  }

  selectGroup (groupId): Observable<Group> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'sport-groups/' + groupId, creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let group = resp.json()
        this._group.next(group);
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }

  userGroups (): Observable<any> {
    if (this.auth.isUserSignedIn()) {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'sport-groups/user', creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let groups = resp.json()
        this._userGroups.next(this.sort(groups))
      }
    })
    return observable;
    } else {
      this._groups.next([])
      return Observable.from([])
    }
  }

  addGroup (group): Observable<Group> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.post(this.endpoint, 'sport-groups', group, creds)).concatAll().share()

    observable.subscribe(resp => {
      if (resp.status === 200) {
        let groups = this._groups.getValue();
        let group = resp.json().group
        groups.push(group)
        this._groups.next(this.sort(groups))
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json().group : null)
  }

  deleteGroup (index): Observable<Group> {
    let groups = this._groups.getValue();
    let obs = this.auth.getCredentials().map(creds => this.sigv4.del(this.endpoint, `sport-groups/${groups[index].groupId}`, creds)).concatAll().share()

    obs.subscribe(resp => {
      if (resp.status === 200) {
        groups.splice(index, 1)[0]
        this._groups.next(<Group[]>groups)
      }
    })
    return obs.map(resp => resp.status === 200 ? resp.json().group : null)
  }

  completeGroup (index): Observable<Group> {
    let groups = this._groups.getValue();
    let obs = this.auth.getCredentials().map(creds => this.sigv4.put(
      this.endpoint,
      `groups/${groups[index].groupId}`,
      {completed: true, completedOn: moment().format(displayFormat)},
      creds)).concatAll().share()

    obs.subscribe(resp => {
      if (resp.status === 200) {
        groups[index] = resp.json().group
        this._groups.next(this.sort(groups))
      }
    })

    return obs.map(resp => resp.status === 200 ? resp.json().group : null)
  }


  private sort (groups:Group[]): Group[] {
    return _orderBy(groups, ['updatedAt', 'createdAt'], ['asc', 'asc'])
  }

  private filter (groups:Group[], key: any, value: any): Group[] {
    groups = _orderBy(groups, ['featured'], ['desc']);
    return (groups) ? groups.filter( item => item[key] === value) : [];
  }
}

export let GroupsStoreProvider = {
  provide: GroupsStore,
  useFactory: groupstoreFactory,
  deps: [Sigv4Http, AuthenticationService, AwsConfig]
}


