import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import 'rxjs/add/operator/share'

import { Sigv4Http } from '../providers/sigv4.service';
import { AuthenticationService } from '../providers/user';
import { AwsConfig } from '../app/app.config';
import { Storage } from '@ionic/storage';

let newsstoreFactory = (sigv4: Sigv4Http, auth: AuthenticationService, config: AwsConfig, storage: Storage) => { return new NewsStore(sigv4, auth, config, storage) }

const displayFormat = 'YYYY-MM-DD'
declare var AWS: any;

@Injectable()
export class NewsStore {
  private _news: BehaviorSubject<any> = new BehaviorSubject([]);
  private endpoint:string;

   constructor (
     private sigv4: Sigv4Http, 
     private auth: AuthenticationService, 
     private config: AwsConfig,
     private storage: Storage) {
     this.endpoint = this.config.get('APIs')['group-newsCRUD'];
   }

    get news () { return Observable.create( fn => this._news.subscribe(fn) ) }


  getGroupNews(groupId: string): Observable<any> {
    let observable = this.auth.getCredentials().map(creds => this.sigv4.get(this.endpoint, 'group-news/' + groupId, creds)).concatAll().share()
    observable.subscribe(resp => {
      if (resp.status === 200) {
        let data = resp.json()
        this._news.next(data);
      }
    })
    return observable.map(resp => resp.status === 200 ? resp.json() : null)
  }


}


export let NewsStoreProvider = {
  provide: NewsStore,
  useFactory: newsstoreFactory,
  deps: [Sigv4Http, AuthenticationService, AwsConfig, Storage]
}
