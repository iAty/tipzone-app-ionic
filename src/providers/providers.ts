// import { Cognito } from './aws.cognito';
import { DynamoDB } from './aws.dynamodb';
import { AuthenticationService } from './user';

export {
  // Cognito,
  DynamoDB,
  AuthenticationService
};
