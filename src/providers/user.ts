import { Injectable } from '@angular/core';
import { CognitoUser, CognitoUserPool, CognitoUserAttribute, AuthenticationDetails, ICognitoUserPoolData, CognitoUserSession } from 'amazon-cognito-identity-js'
export const _USER_LOGOUT_EVENT = 'user:logout';
export const _USER_LOGIN_EVENT = 'user:login';
import { Storage } from '@ionic/storage';

import { AuthService, SocialUser, GoogleLoginProvider } from 'angularx-social-login';
import { FacebookLoginProvider } from "angularx-social-login";


import { Subject } from 'rxjs/Subject'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/from'
import { AwsConfig } from '../app/app.config';
import { NavController } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { List } from 'immutable';
import { DatabaseService } from './db';
import { DynamoDB } from './providers';

declare var AWS: any;

@Injectable()
export class AuthenticationService {
  private s3: any;
  private socialUser: SocialUser;
  private unauthCreds: any
  private poolData: ICognitoUserPoolData
  private userPool: CognitoUserPool
  private _cognitoUser: CognitoUser
  private session: CognitoUserSession
  private _signoutSubject: Subject<string> = new Subject<string>()
  private _signinSubject: Subject<string> = new Subject<string>()
  private _favorites: Array<any> = new Array();
  public favorites: BehaviorSubject<any> = new BehaviorSubject([]);

  public loggedIn: boolean = false;

  constructor(
    private config: AwsConfig,
    private storage: Storage,
    private authService: AuthService,
    private databaseService: DatabaseService,
    private ddb: DynamoDB) {

    AWS.config.region = this.config.get('region')
    this.poolData = { UserPoolId: this.config.get('userPoolId'), ClientId: this.config.get('appId') }
    this.userPool = new CognitoUserPool(this.poolData)
    this.refreshOrResetCreds();
    this.storage.get('favorites').then( favorites => this.favorites.next(this._favorites = (favorites) ? favorites : []));
    
    this.authService.authState.subscribe((user) => {
      this.socialUser = user;
      this.loggedIn = (user != null);
      if (user != null) {
      AWS.config.credentials = new AWS.WebIdentityCredentials({
          RoleArn: 'arn:aws:iam::233995683236:role/service-role/AMZPinpointAccessRole',
          ProviderId: 'graph.facebook.com',
          WebIdentityToken: user.authToken
        });
        this.refreshOrResetCreds();
        // window.location.reload();
      }
      
    });
  }

  get signoutNotification() { return Observable.create(fn => this._signoutSubject.subscribe(fn)) }
  get signinNotification() { return Observable.create(fn => this._signinSubject.subscribe(fn)) }

  // get favorites() { return Observable.create(fn => this._favorites.subscribe(fn)) }
  get cognitoUser(): CognitoUser { return this._cognitoUser }
  get currentIdentity(): string { return AWS.config.credentials.identityId }
  isUserSignedIn(): boolean { return this._cognitoUser !== null }

  private refreshOrResetCreds() {
    // console.log('refreshOrResetCreds')
    this._cognitoUser = this.userPool.getCurrentUser()
    // console.log('refreshOrResetCreds', this._cognitoUser);
    this.s3 = new AWS.S3({
      'params': {
        'Bucket': this.config.get('s3UserBucket')
      },
      'region': this.config.get('s3UserRegion')
    });

    if (this._cognitoUser !== null) {
      this.refreshSession()
    } else {
      this.resetCreds()
    }

    
  }

  private setCredentials(newCreds) {
    AWS.config.credentials = newCreds
  }

  private buildLogins(token) {
    let key = this.config.get('idpURL') + '/' + this.config.get('userPoolId')
    let json = { IdentityPoolId: this.config.get('identityPool'), Logins: {} }
    json.Logins[key] = token
    return json
  }

  // private buildFBLogins(token) {
  //   let key = 'graph.facebook.com';
  //   let json = { IdentityPoolId: this.config.get('identityPool'), Logins: {} }
  //   json.Logins[key] = token
  //   return json
  // }

  private buildCreds() {
    let json = this.buildLogins(this.session.getIdToken().getJwtToken())
    return new AWS.CognitoIdentityCredentials(json)
  }


  private saveCreds(session, cognitoUser?): void {
    this.session = session
    if (cognitoUser) { this._cognitoUser = cognitoUser }
    this.setCredentials(this.buildCreds())
  }

  private getNewCognitoUser(creds): CognitoUser {
    return new CognitoUser({ Username: creds.username, Pool: this.userPool })
  }

  private authDetails(creds): AuthenticationDetails {
    return new AuthenticationDetails({ Username: creds.username, Password: creds.password })
  }

  private refreshSession(): Promise<CognitoUserSession> {
    let self = this
    return new Promise((resolve, reject) => {
      self._cognitoUser.getSession((err, session) => {
        if (err) { console.warn('Error refreshing user session', err); return reject(err) }
        // console.log(`${new Date()} - Refreshed session for ${self._cognitoUser.getUsername()}. Valid?: `, session.isValid())
        self.saveCreds(session)
        resolve(session)
      })
    })
  }

  private resetCreds(clearCache: boolean = false) {
    // console.log('Resetting credentials for unauth access')
    AWS.config.region = this.config.get('region')
    this._cognitoUser = null
    this.unauthCreds = this.unauthCreds || new AWS.CognitoIdentityCredentials({ IdentityPoolId: this.config.get('identityPool') })
    if (clearCache) { this.unauthCreds.clearCachedId() }
    this.setCredentials(this.unauthCreds)
  }

  private buildAttributes(creds): Array<CognitoUserAttribute> {
    let attributeList = []
    let attributeEmail = new CognitoUserAttribute({ Name: 'email', Value: creds.email })
    let attributeName = new CognitoUserAttribute({ Name: 'preferred_username', Value: creds.username })
    attributeList.push(attributeEmail)
    attributeList.push(attributeName)
    return attributeList
  }

  private _getCreds(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        AWS.config.credentials.get((err) => {
          if (err) { return reject(err) }
          resolve(AWS.config.credentials)
        })
      } catch (e) { reject(e) }
    })
  }

  addOrRemoveFavorite(newFavoriteGroup: any): void {
    // add new favorite
    let newFavoriteItem: any = {
      name: newFavoriteGroup.groupName || newFavoriteGroup.name,
      id: newFavoriteGroup.groupId || newFavoriteGroup.id,
      seasonId: newFavoriteGroup.seasonId,
      season: newFavoriteGroup.seasonName || newFavoriteGroup.season,
      featured: newFavoriteGroup.featured
    }

    if (!this._favorites) {
      this._favorites = [];
    }

    if (newFavoriteItem.id === undefined) {
      // fallback for any case of error
      return;
    }

    if (!this._favorites.find(item => item.id === newFavoriteItem.id)) {
      // console.log('NOT INCLUDES');
      this._favorites.push(newFavoriteItem);
    } else if (this._favorites.find(item => item.id === newFavoriteItem.id)) {
      // console.log('INCLUDES');
      this._favorites = this._favorites.filter( elem => elem.id !== newFavoriteItem.id);
    }

    this._favorites = this.removeDuplicates(this._favorites, 'id');
    this.favorites.next(this._favorites);
    this.storage.set('favorites', this._favorites);
  }

  hasFavorite(newFavoriteGroup: any): boolean {
    let newFavoriteItem: any = {
      name: newFavoriteGroup.groupName,
      id: newFavoriteGroup.groupId,
      seasonId: newFavoriteGroup.seasonId,
      season: newFavoriteGroup.seasonName,
      featured: newFavoriteGroup.featured
    }

    return this._favorites.find(mapObj => {
      if (mapObj.id === newFavoriteItem.id) {
        return true;
      }
    });
  }

  getFavorites(): Promise<any> {
    return this.storage.get('favorites');
  }

  isAuthenticated(): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log('this.isUserSignedIn()', this.isUserSignedIn());
      if (this.isUserSignedIn()) {
        this.refreshOrResetCreds();
        resolve(true);
      } else {
        reject(false);
      }
    });
  }

  getUserAttributes(callback): any {
    this.userPool.getCurrentUser().getUserAttributes(callback);
  }

  getCredentials(): Observable<any> {
    let result = null
    if (this._cognitoUser === null) { result = this._getCreds() }
    else if (this.session && this.session.isValid()) { result = this._getCreds() }
    else { result = this.refreshSession().then(this._getCreds) }
    return Observable.from(result)
  }

  signout() {
    if (this._cognitoUser) {
      let name = this._cognitoUser.getUsername()
      this._cognitoUser['signOut']()
      this.resetCreds(true)
      this._signoutSubject.next(name)
    }
  }

  register(creds): Promise<CognitoUser> {
    let self = this
    return new Promise((resolve, reject) => {
      try {
        self.userPool.signUp(creds.username, creds.password, self.buildAttributes(creds), null, (err, result) => {
          if (err) { return reject(err) }
          // console.log('Register', result)
          resolve(result.user)
        })
      } catch (e) { reject(e) }
    })
  }

  resetUserPassword(username):Promise<any> {
    let cognitoUser = new CognitoUser({ Username: username, Pool: this.userPool });
    return new Promise((resolve, reject) => {

      try {
        // console.log('Confirming...', cognitoUser);
        cognitoUser.forgotPassword({
          onSuccess: function (result) {
              // console.log('call result: ' + result);
              resolve(result);
          },
          onFailure: function(err) {
            reject(err);
          },
          inputVerificationCode() {
              var verificationCode = prompt('Please input verification code ' ,'');
              var newPassword = prompt('Enter new password ' ,'');
              cognitoUser.confirmPassword(verificationCode, newPassword, this);
              resolve('reset sended NOW!');
          }
        });
      } catch (e) { reject(e) }

    });
  }

  resendRegistrationCode(username) {

    return new Promise((resolve, reject) => {
      let user = new AWS.CognitoIdentityServiceProvider.CognitoUser({
        'Username': username,
        'Pool': this.config.get('userPoolId')
      });
      user.resendConfirmationCode((err, result) => {
        if (err) {
          // console.log('could not resend code..', err);
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  confirm(creds): Promise<CognitoUser> {
    let cognitoUser = this.getNewCognitoUser(creds)
    return new Promise((resolve, reject) => {
      try {
        // console.log('Confirming...', CognitoUser)
        cognitoUser.confirmRegistration(creds.confcode, true, (err, result) => {
          if (err) { return reject(err) }
          resolve(result.CognitoUser)
        })
      } catch (e) { reject(e) }
    })
  }
  /**
   * @todo: finish FB login
   * https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/config-web-identity-examples.html
   */
  loginFacebook(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(userData => {
        AWS.config.credentials = new AWS.WebIdentityCredentials({
          RoleArn: 'arn:aws:iam::233995683236:role/service-role/AMZPinpointAccessRole',
          ProviderId: 'graph.facebook.com',
          WebIdentityToken: userData.authToken
        });
        this.refreshOrResetCreds();
        resolve(userData);
      }).catch(hasError => reject(hasError));
    })

  }

  loginGoogle(): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
        .then(userData => {
          AWS.config.credentials = new AWS.WebIdentityCredentials({
            RoleArn: 'arn:aws:iam::233995683236:role/service-role/AMZPinpointAccessRole',
            ProviderId: 'accounts.google.com',
            WebIdentityToken: userData.authToken
          });
          this.refreshOrResetCreds();
          resolve(this.cognitoUser);
        }).catch(hasError => reject(hasError));
    })
  }

  login(creds): Promise<CognitoUser> {
    let cognitoUser = this.getNewCognitoUser(creds)
    let self = this
    return new Promise((resolve, reject) => {
      try {
        cognitoUser.authenticateUser(self.authDetails(creds), {
          onSuccess: (session) => {
            // console.log(`Signed in user ${cognitoUser.getUsername()}. Sessiong valid?: `, session.isValid())
            self.saveCreds(session, cognitoUser)
            self._signinSubject.next(cognitoUser.getUsername())
            resolve(cognitoUser)
          },
          newPasswordRequired: (userAttributes, requiredAttributes) => {
            // console.log('newPasswordRequired', userAttributes, requiredAttributes);
          },
          mfaRequired: (challengeName, challengeParameters) => {
            // console.log('mfaRequired', challengeName, challengeParameters);
          },
          customChallenge: (challengeParameters) => {
            // console.log('customChallenge', challengeParameters);
          },
          onFailure: reject
        })
      } catch (e) { reject(e) }
    })
  }

  goToLogin(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.storage.ready().then(() => {
        this.storage.get('hasSeenTutorial').then(hasSeen => {
          this.storage.set('user_login_attempt', true);
          if (hasSeen) {
            resolve('tabs');
          } else {
            resolve('tutorial');
          }
        }).catch(storageError => console.warn(storageError))
        resolve('tabs')
      });
    });
  }

  refreshAvatar(): Promise<any> {
    return new Promise((resolve, reject) => {
    this.getCredentials().subscribe( hasCreds => {
        this.s3.getSignedUrl('getObject', { 'Key': 'public/user/' + AWS.config.credentials.identityId + '/avatar.jpg' }, (err, url) => {
          if (err) { reject(err); }
          resolve(url);
        });
      }, hasError => {
          console.warn('auth error creds', hasError);
          reject(hasError);
        });
    });
  }

  private removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
}



export class GenericUserDetails {
  country: string;
  language: string;
  credit: number;
  emailNotification: boolean;
  newsletter: boolean;
  pushNotification: boolean;
  updateAt: string;
  userId: string;
  username: string;
  familyName: string;
  givenName: string;
  avatar: string;
  provider: string;
  // authToken
  // email
  // id

    constructor(data: any) {
      this.username = data.name;
      this.givenName = data.firstName;      
      this.familyName = data.lastName;
      this.avatar = data.photoUrl;
      this.provider = data.provider;
    }
}
