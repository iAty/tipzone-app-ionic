
import { Injectable } from '@angular/core';

import { Config } from 'ionic-angular';
import { DynamoDB } from './aws.dynamodb';
import { environment } from '../environments/environment';

declare var AWS: any;

export class Stuff {
  public type: string;
  public date: string;
}

@Injectable()
export class LogEntryService {

  constructor( private db: DynamoDB) {}

  getLogEntries(mapArray: Array<Stuff>) {
    // console.log("DynamoDBService: reading from DDB with creds - " + AWS.config.credentials);
    var params = {
      TableName: environment.LOG_ENTRY_TABLE,
      KeyConditionExpression: "userId = :userId",
      ExpressionAttributeValues: {
        ":userId": AWS.config.credentials.identityId
      },
      Limit: 10
    };

    var clientParams: any = {};

    var docClient = this.db.getDocumentClient();
    docClient.query(params, onQuery);

    function onQuery(err, data) {
      if (err) {
        console.error("DynamoDBService: Unable to query the table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
        // print all the movies
        // console.log("DynamoDBService: Query succeeded.");
        data.Items.forEach(function (logitem) {
          mapArray.push({ type: logitem.type, date: logitem.activityDate });
        });
      }
    }
  }

  writeLogEntry(type: string, optionaldata?: any) {
    try {
      let date = new Date().toString();
      // console.log("DynamoDBService: Writing log entry. Type:" + type + " ID: " + AWS.config.credentials.identityId + " Date: " + date);
      this.write(AWS.config.credentials.identityId, date, type, optionaldata).then( succ => console.info('success', succ)).catch(hasError => console.warn('has error', hasError));
    } catch (e) {
      // console.log("DynamoDBService: Couldn't write to DDB", e);
    }

  }

  write(data: string, date: string, type: string, optionaldata?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let EntryItem = {
        userId: data,
        activityDate: date,
        type: type
      };
      if (optionaldata) {
        EntryItem = Object.assign({}, EntryItem, optionaldata);
      }
      this.db.getDocumentClient().put({
          TableName: environment.LOG_ENTRY_TABLE,
          Item: EntryItem
        }).promise().then( result => {
        // console.log("DynamoDBService: wrote entry: " + JSON.stringify(result));
        resolve(result);
      }).catch((err) => {
        // console.log('DynamoDBService error: ', err);
        resolve(err);
      });
    });
  }

}
