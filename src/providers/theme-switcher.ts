import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ThemeSwitcherProvider {

  private switch: Subject<string> = new Subject();
  private background: Subject<any> = new Subject();

  constructor() {
  }

  watch(): Observable<string> {
    return this.switch.asObservable();
  }

  watchBackground(): Observable<any> {
    return this.background.asObservable();
  }

  switchColor(theme: string) {
    this.switch.next(theme);
  }

  switchBackground(backgroundUrl: any) {
    this.background.next(backgroundUrl);
  }

}
