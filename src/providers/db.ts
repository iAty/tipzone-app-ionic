import { Injectable } from '@angular/core';

// import { Cognito } from './aws.cognito';
import { Config } from 'ionic-angular';
import { DynamoDB } from './aws.dynamodb';
// import * as _first from 'lodash/first';
import * as _groupBy from 'lodash.groupby';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
declare var AWS: any;
// declare const aws_cognito_region;
// declare const aws_cognito_identity_pool_id;
// declare const aws_user_pools_id;
// declare const aws_user_pools_web_client_id;

@Injectable()
export class DatabaseService {

  groups = {
    Items: [
      {
        groupId: 12,
        user: 'Tipper_hu',
        name: 'MLSZ OTP Bank liga',
        description: 'loremIpsum',
        image: null,
        closed: false,
        users: [
          'userid_here'
        ],
        joined: 238,
        active: false
      },
      {
        groupId: 22,
        user: 'szunyog83bp',
        name: 'GRUPAMA liga',
        description: 'loremIpsum',
        image: null,
        closed: false,
        users: [
          'userid_here'
        ],
        joined: 12,
        active: false
      },
      {
        groupId: 32,
        user: 'vodafone',
        name: 'Vodafone liga',
        description: 'loremIpsum',
        image: null,
        closed: true,
        users: [
          'userid_here'
        ],
        joined: 33
      },
      {
        groupId: 42,
        user: 'Bela234',
        name: 'Telenor liga',
        image: '/assets/groups/testuser_avatar.jpeg',
        description: 'loremIpsum',
        closed: true,
        users: [
          'userid_here'
        ],
        joined: 44,
        active: false
      },
      {
        groupId: 52,
        user: 'Tipper_hu',
        name: 'MLSZ OTP Bank liga',
        description: 'loremIpsum',
        image: null,
        closed: true,
        users: [
          'userid_here'
        ],
        joined: 88,
        active: false
      }
    ]
  };

  userLocalConfig: any;

  constructor(
    // public cognito: Cognito,
    public config: Config,
    private db: DynamoDB,
    private http: HttpClient,
    public storage: Storage
  ) {

  }


  getResults(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('/assets/database/results.json')
      .subscribe( data => {
        resolve(data);
      })
    })
  }


  getAllGroups(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('/assets/database/groups.json')
      .subscribe( data => {
        resolve(data);
      })
      // this.db.getDocumentClient().scan({
      //   'TableName': 'tipzone-mobilehub-1836564323-groups',
      //   'IndexName': 'UserByGameIndex',
      //   // 'KeyConditionExpression': '#userId = :userId',
      //   // 'ExpressionAttributeNames': {
      //   //   '#userId': 'userId',
      //   // },
      //   // 'ExpressionAttributeValues': {
      //   //   ':userId': AWS.config.credentials.identityId
      //   // },
      //   'ScanIndexForward': false
      // }).promise().then((data) => {
      //   resolve(data);
      // }).catch((err) => {
      //   // console.log(err);
      // });
    })
  }

  getGroupsByUser(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getDocumentClient().scan({
        'TableName': 'tipzone-mobilehub-1836564323-group-data',
        'FilterExpression': 'userId = :userId',
        'ExpressionAttributeValues': {
          ':userId': AWS.config.credentials.identityId
        },
        'ScanIndexForward': false
      }).promise().then((data) => {
        resolve(data.Items);
      }).catch((err) => {
        // console.log(err);
      });
    })
  }

  getUserConfig(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.ready().then( isReady => {
        this.storage.get('user_config').then( userCurrentConfig => {
          this.db.getDocumentClient().query({
            'TableName': 'tipzone-mobilehub-1836564323-user-custom-config',
            'KeyConditionExpression': 'userId = :userId',
            'ExpressionAttributeValues': {
              ':userId': AWS.config.credentials.identityId
            }
          }).promise().then((data) => {
            let allUsertConfig = Object.assign({}, userCurrentConfig, data.Items[0])
            resolve(allUsertConfig);
          }).catch((err) => {
            // console.log(err);
          });
        })
      })
    })
  }

  setUserConfig(Item: any): Promise<any> {
    Item.userId = AWS.config.credentials.identityId;
    return new Promise((resolve, reject) => {
      this.db.getDocumentClient().put({
        'TableName': 'tipzone-mobilehub-1836564323-user-custom-config',
        'Item': Item
      }).promise().then((data) => {
        resolve(data);
      }).catch((err) => {
        // console.log(err);
        reject(err);
      });
    })
  }


  getOneGroupById(groupId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getDocumentClient().query({
        'TableName': 'tipzone-mobilehub-1836564323-group-data',
        'KeyConditionExpression': 'groupId = :groupId',
        'ExpressionAttributeValues': {
          ':groupId': groupId
        },
        'ScanIndexForward': false
      }).promise().then((data) => {
        resolve(data.Items[0]);
      }).catch((err) => {
        // console.log(err);
      });
    })
  }

  getStandingBySeasonId(seasonId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getDocumentClient().scan({
        'TableName': 'tipzone-mobilehub-1836564323-standings-new',
        'FilterExpression': 'seasonId = :seasonId',
        'ExpressionAttributeValues': {
          ':seasonId': seasonId
        },
        'ScanIndexForward': false
      }).promise().then((data) => {
        resolve(data.Items);
      }).catch((err) => {
        // console.log(err);
      });
    })
  }

  getUsersStanding(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (AWS.config.credentials.identityId){
      this.db.getDocumentClient().scan({
        'TableName': 'tipzone-mobilehub-1836564323-user-results',
        'FilterExpression': 'userId = :userId',
        'ExpressionAttributeValues': {
          ':userId': AWS.config.credentials.identityId
        },
        'ScanIndexForward': false
      }).promise().then((data) => {
        let results = _groupBy(data.Items, 'seasonId');
        let resultOutput = {};
        for (let season of Object.keys(results)) {
          if (resultOutput[season] === undefined) {
            resultOutput[season] = {};
          }

          for( let match of results[season]) {
            resultOutput[season][match.resultId] = match;
          }
        }
        resolve(resultOutput);
      }).catch((err) => {
        // console.log(err);
      });
      }
    })
  }


  setUserPickByResultId(pickItem: any, reset?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      pickItem.userId = AWS.config.credentials.identityId;
      this.db.getDocumentClient().get({
        'TableName': 'tipzone-mobilehub-1836564323-user-results',
        'IndexName': 'resultId-userId-index',
        'Key': {
          'resultId': pickItem.resultId,
          'userId': pickItem.userId
        }
      }).promise()
      .then((data) => {
        // console.log('success', data);
        if (data.Item) {
          return data.Item;
        }
        return {};
      })
      .then(isAvailable=>{

        if (isAvailable.createdAt) {
          pickItem.createdAt = isAvailable.createdAt;
        }
        pickItem.pickCount = (isAvailable.pickCount) ? isAvailable.pickCount+1 : 1;
        pickItem.locked = reset || true;

        this.db.getDocumentClient().put({
          'TableName': 'tipzone-mobilehub-1836564323-user-results',
          'Item': pickItem
        }).promise().then(() => {
          // console.log('setUserPickByResultId success');
          resolve(true);
        }).catch((err) => {
          // console.log('setUserPickByResultId error', err);
        });
      })
      .catch( fail => console.warn('fail', fail));

    })
  }

}
