// 93dee817f9d350b6fe49451601fab78039a25f6055e5d262902870003c0c8fa4

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

const UNSPALSH_APP_ID = '25daadf117924d95970267457470cdd776fe51e2a0bb64a6b42e62bed2103e32';
const UNSPLASH_API_URL = 'https://api.unsplash.com';
/**
* This class provides the UnsplashAP service with methods to read names and add names.
*/
@Injectable()
export class UnsplashAPIService {


/**
* Creates a new UnsplashAPService with the injected Http.
* @param {Http} http - The injected Http.
* @constructor
*/
  constructor(private http: HttpClient, private storage: Storage) {}
/**
* Returns an Observable for the HTTP GET request for the JSON resource.
* @return {string[]} The Observable for the HTTP request.
*/
  get(collectionId: number): Promise<string[]> {
    return new Promise( (resolve, reject) => {
    this.storage.get('unspalsh').then( (hasUnsplashCache: string[]) => {
      if (hasUnsplashCache) {
      resolve(hasUnsplashCache);
    } else {

      const PATH = '/collections/'+collectionId+'/photos'
      this.http.get(UNSPLASH_API_URL + PATH + `?client_id=${UNSPALSH_APP_ID}`)
      .map((res: any) => res).subscribe( items => {
        this.storage.set('unspalsh', items).then( success => {
          resolve(items);
        });
      })
    }
    }).catch( needToGetUnsplash => {
      reject("Server error");
    })
    });
  }
}
