export class LoginDetails {
  username: string;
  password: string;
}

export class UserDetails {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
}
