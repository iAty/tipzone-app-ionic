export interface Result {
  scheduled: string
  resultId: string
  competitors: any
  season: any
  start_time_tbd: boolean
  status: string
  tournament: any
  tournament_round: any
  venue: any
}
