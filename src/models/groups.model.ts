export interface Group {
  userId?:string
  createdAt:number
  updatedAt:number
  featured?:boolean
  hasCustomStyle:boolean
  joinLimit?:number
  gameId?:string
  gameName?:string
  name?:string
  type?:string
  groupId:string
}
