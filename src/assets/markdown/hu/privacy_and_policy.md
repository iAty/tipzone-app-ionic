# ADATVÉDELMI IRÁNYELVEK

Dátum: 2018. junius 6.

### KÉRJÜK, HOGY EZEN A WEBOLDAL HASZNÁLATA ELŐTT OLVASSA EL EZT A POLITIKA TEKINTETÉBEN

Az adatok, az adatvédelem és a pénzügyi adatok védelme nagyon fontos
TipZone Kft. ("**TipZone**", "**us**" vagy "**we**").

Ez a házirend (a [Felhasználási feltételek] (https://www.tipzone.net/terms) és bármely más
az abban hivatkozott dokumentumok) meghatározza az alapját, amely alapján minden személyes
az Ön által gyűjtött adatokat, vagy az Ön által megadott adatokat feldolgozzuk
általunk. Kérjük, olvassa el figyelmesen az alábbiakat, hogy megértse a
információkat gyűjtünk öntől, hogyan használjuk ezeket az információkat és a
körülményeket, amelyek alapján harmadik felekkel megosztjuk. Által
látogasson el weboldalunkra (a "**weboldal**"), amelyet elfogad és
beleegyezik a jelen irányelvben ismertetett gyakorlatokba.

A vonatkozó adatvédelmi jogszabályok alkalmazásában az adatokat
vezérlő TipZone Limited a 7 Bodmin Road, Chelmsford, Essex,
Anglia, CM1 6LH.

## Információkat gyűjthetünk Öntől

A következő adatokat gyűjthetjük és feldolgozhatjuk Önről:

* **Az Ön által megadott adatok** Ön adhat nekünk információkat Önről
az űrlap kitöltésével a weboldalunkon, vagy a megfelelő módon telefonon,
e-mailben vagy más módon. Ez magában foglalja (de nem kizárólag) az információkat
akkor jelentkezz be, amikor regisztrálsz a weboldal használatára, iratkozz fel
szolgáltatás, pénz átutalása a weboldalunkon keresztül, és a probléma bejelentésekor
a honlapunkon. Az Ön által megadott adatok magukban foglalhatják nevét,
cím, e-mail cím és telefonszám, pénzügyi és hitelkártya
információ, személyes leírás, földrajzi hely, fénykép,
a társadalombiztosítási szám és az azonosító okmányok másolata.

* **Információkat gyűjtünk Önről.** Minden egyes látogatásra vonatkozóan
honlapunkra automatikusan a következő információkat gyűjti össze:

    * a honlapunkon keresztül végrehajtott pénzátutalások részletei, beleértve
a földrajzi hely, ahonnan a tranzakció származik;

    * technikai információk, beleértve az internet protokoll (IP) címet is
a számítógépet az internethez, a bejelentkezési adatait,
a böngésző típusa és verziója, az időzóna beállítása, a böngészőbe ágyazott típusok és a
verziók, operációs rendszerek és platform; és

    * az Ön látogatása, beleértve a teljes Uniform Resource-ot is
Az URL-ek keresője (URL) a webhelyünkre kattintva, a webhelyünkön keresztül és onnan
dátum és idő); amit megtekintett vagy keresett; oldal válaszidő,
letöltési hibák, bizonyos oldalak látogatásának időtartama, oldal interakció
információkat (például görgetést, kattintásokat és egérmutatókat) és módszereket
az oldalról történő böngészéshez és a híváshoz használt bármely telefonszámhoz használják
ügyfélszolgálati szám.

* **Más forrásokból kapott információk**
információt arról, ha bármelyik másik weboldalunkat működtetjük vagy
a többi szolgáltatásunkat. Szorosan együtt dolgozunk a harmadiknál
pártok (ideértve például az üzleti partnereket, a
műszaki, fizetési és kézbesítési szolgáltatások, hirdetési hálózatok,
elemző szolgáltatók, keresési információ szolgáltatók, hitel hivatkozás
ügynökségek), és róluk információkat kaphatnak tőlük.

## Sütik

Webhelyünk cookie-kat használ, hogy megkülönböztessük Önt a többi felhasználótól
Weboldal. Ez segít nekünk abban, hogy jó élményt nyújtson Önnek
böngészhet honlapunkon, és lehetővé teszi számunkra, hogy javítsuk weboldalunkat. mert
részletes információkat az általunk használt cookie-król és azokról a célokról, amelyekről mi
használja őket a [Cookie policy] (https://www.tipzone.net/cookie_policy) oldalon.

## Az információkból származó felhasználások

Az Ön számára tartott információkat az alábbi módokon használjuk:

* **Az Ön által megadott adatok** Ezt az információt felhasználjuk:

    * az elvégzett szerződésekből eredő kötelezettségeink teljesítése
köztünk és magunk között, és biztosítsuk Önnek az információkat, a termékeket és a termékeket
Ön által igényelt szolgáltatások;

    * hogy információt szolgáltasson az általunk kínált egyéb árukról és szolgáltatásokról
amelyek hasonlóak a már megvásárolt vagy megkérdezettekéhez
ról ről;

    * az Ön számára, vagy lehetővé teszi a kiválasztott harmadik felek számára, hogy Önnek
az olyan árukról vagy szolgáltatásokról szóló információk, amelyekről úgy érzed, hogy érdekelhetnek;

    * értesíteni Önt a szolgáltatás megváltoztatásáról;

    * annak biztosítására, hogy a weboldalunkon található tartalom leginkább megjelenjen
hatékony módja az Ön számára és számítógépének vagy mobileszközének

    * személyre szabott felhasználói élmény;

    * felmérések és promóciók elvégzésére ;.

* **Információkat gyűjtünk Önről.** Ezt az információt felhasználjuk:

    * weboldalunk és belső működésének kezelésére, beleértve
hibaelhárítás, adatelemzés, tesztelés, kutatás, statisztikai és
felmérési célok;

    * Weboldalunk és ügyfélszolgálatunk fejlesztése a tartalom biztosítása érdekében
a leghatékonyabban bemutatva az Ön és a számítógépén vagy
mobil eszköz

    * hogy lehetővé tegye szolgáltatásaink interaktív szolgáltatásain való részvételt, amikor
úgy döntesz, hogy ezt megteszi, és feldolgozza az ügyleteket;

    * azon erőfeszítéseink részeként, amelyek biztonságossá teszik honlapunkat;

    * javaslatokat és ajánlásokat tehet Önnek és más felhasználóknak
A termékekre és szolgáltatásokra vonatkozó weboldal, amelyek érdekelhetik Önt vagy őket.

* **Más forrásokból kapott információk** Ezzel kombinálhatjuk
az Ön által megadott adatokkal és az általunk gyűjtött információkkal
rólad. Ezt az információt és a kombinált információkat használhatjuk
a fenti célok (az információ típusától függően)
kap).

## Az Ön információinak közlése

Megoszthatjuk személyes adatait csoportunk bármely tagjával,
ami leányvállalatainkat, a végső holdingtársaságunkat és a miét jelenti
leányvállalatok, ahogyan azt a brit társaságokról szóló 2006. évi törvény 1159. szakasza határozza meg.

Megoszthatjuk az adatokat harmadik felekkel, többek között:

* üzleti partnerek, beszállítók és alvállalkozók a
bármilyen szerződést kötöttünk velük vagy önnel;

* hirdetők és hirdetési hálózatok, amelyekhez az adatok kiválasztása és kiválasztása szükséges
releváns hirdetéseket jelenítsen meg Önnek és másoknak.

* elemző és keresőmotor-szolgáltatók, amelyek segítenek nekünk a javulásban
weboldalunk optimalizálása; és

Személyes adatait harmadik felek számára közzé teheti:

* abban az esetben, ha üzletet vagy vagyont vásárolunk vagy veszünk, mely esetben
személyes adatainkat nyilvánosságra hozhatjuk a lehetséges eladónak vagy vásárlónak
ilyen üzlet vagy eszközök;

* ha a TipZone vagy lényegében az összes vagyonát egy harmadiknál ​​szerezte meg
amely esetben az ügyfelei által birtokolt személyes adatok kerülnek
az egyik átruházott eszköz;

* ha kötelességünk a személyes adatait közzétenni vagy megosztani
köteles betartani minden jogi kötelezettséget és minden egyéb hivatkozott dokumentumot
Rajta; vagy védjük a TipZone, a mi jogainkat, tulajdonunkat vagy biztonságunkat
felhasználók vagy mások. Ez magában foglalja az információcserét másikkal
cégek és szervezetek csalás elleni védelem céljából
hitelkockázat csökkentés.

Elképzelhető, hogy személyes adatait (például vezetéknevét,
a vezetéknév és a fénykép első kezdete) harmadik félnek (
például a címzett vagy az egyenrangú partnere) annak érdekében, hogy
pénzátutalást vagy kérelmet teljesít. Használhatjuk a saját adatait is
tranzakció az Ön keresztneve, első vezetéknevének kezdete
és fénykép marketing és reklám célokra, kivéve ha Önnek
arra utasít minket, hogy ne.

## Hol tároljuk személyes adatait

Az általunk gyűjtött adatokat át lehet adni és tárolni,
az Európai Gazdasági Térségen kívüli rendeltetési hely ("** EGT **"). Lehet
az EGT - n kívül működő személyzet is feldolgozhatja, aki vagy nekünk vagyunk
az egyik beszállítónk számára. Az ilyen munkatársak többek között részt vehetnek
a dolgok, a pénzátutalás teljesítése, az Ön feldolgozása
a fizetési adatok és a támogatási szolgáltatások nyújtása. Benyújtásával
az Ön személyes adatait, Ön elfogadja ezt az átruházást, tárolást vagy feldolgozást.
Minden szükséges lépést megteszünk annak érdekében, hogy az Ön adatai legyenek
biztonságosan kezelve és az adatvédelmi irányelvnek megfelelően.

Az Ön által megadott összes információ a biztonságos szervereken tárolódik. Bármilyen
a fizetési tranzakciók titkosításra kerülnek a Transport Layer Security használatával
(TLS). Hol adott meg neked (vagy ahol választottál) jelszót
amely lehetővé teszi számodra, hogy hozzáférj weboldalunk bizonyos részeihez, te vagy
felelős azért, hogy titkolja ezt a jelszót. Azt kérjük tőled, hogy ne
ossza meg a jelszavát bárkinek.

Sajnos az interneten keresztüli információ továbbítása nem
teljesen biztonságos. Bár mindent meg fogunk tenni, hogy megvédjük személyes adatait
adatokat nem tudunk garantálni a mi adataink biztonságáért
Weboldal; minden adatátvitel saját felelősségére történik. Miután megkaptuk
az Ön információi, szigorú eljárásokat és biztonsági funkciókat használunk
hogy megakadályozzák a jogosulatlan hozzáférést.

Korlátozzuk személyes adataid elérését a (z)
TipZone, akinek üzleti oka van ennek az információnak a megismerésében. Mi
folyamatosan oktassuk és képezzük munkatársainkat a munkaerő fontosságáról
titkosságát és az ügyféladatok adatainak védelmét. Fenntartjuk
fizikai, elektronikus és eljárási biztosítékokat, amelyek megfelelnek a
a személyes adatok védelme érdekében vonatkozó törvények és rendelkezések
minden jogosulatlan hozzáférést.

## Jogok

Önnek jogában áll kérni tőlünk, hogy ne dolgozzon fel személyes adatait
marketing célokra. Általában tájékoztatjuk Önt (mielőtt összegyűjtené
adatokat), ha szándékunkban áll használni az adatait ilyen célokra, vagy ha szándékunk van
az információkat ilyen célokra harmadik felek számára közzé kell tenni. tudsz
gyakorolhatja az ilyen feldolgozás megakadályozásának jogát egyes dobozok ellenőrzésével
az adatgyűjtéshez használt űrlapokon. Ön is gyakorolhatja a
bármikor kapcsolatba léphet velünk a [letsconnect@tipzone.net] címen (mailto: letsconnect@tipzone.net).

Honlapunk időről-időre tartalmazhat hivatkozásokat és a
partnerhálózatok, hirdetők és leányvállalatok weboldalait. Ha te
nyomon követhet egy linket ezekhez a webhelyekhez, kérjük, vegye figyelembe, hogy ezek a webhelyek
saját adatvédelmi politikájuk van, és nem fogadunk el semmit
felelősség vagy felelősség ezekre a politikákra. Kérjük, ellenőrizze ezeket
mielőtt bármilyen személyes adatot elküldenének ezeknek a weboldalaknak.

## Hozzáférés az információhoz

Az alkalmazandó jogszabályoktól függően Önnek joga van hozzáférni
az Önnel kapcsolatos információk. Hozzáférési joga gyakorolható
a vonatkozó adatvédelmi jogszabályokkal összhangban. Bármilyen hozzáférés
a kérelem díjköteles lehet, hogy költségünket az Ön rendelkezésére bocsássuk
az Önnel kapcsolatos információk részleteit.

## harmadik fél weboldalai

A felhasználók hirdetéseket vagy egyéb tartalmakat találhatnak a weboldalunkon, amely linkeket mutat
partnereink, beszállítóink, hirdetőink weboldalai és szolgáltatásai,
szponzorokat, licenctulajdonosokat és más harmadik feleket. Nem ellenőrzik a
tartalmat vagy linkeket, amelyek ezeken a weboldalakon jelennek meg és nem felelnek
a honlapunkhoz kapcsolódó vagy onnan kapcsolódó webhelyek által alkalmazott gyakorlatok. Ban ben
Emellett ezen oldalak vagy szolgáltatások, beleértve azok tartalmát és linkjeit,
folyamatosan változhat. Ezek a webhelyek és szolgáltatások lehetnek sajátjaik
az adatvédelmi irányelvek és az ügyfélszolgálati irányelvek. Böngészés és interakció
bármely más weboldalon, beleértve azokat a weboldalakat is, amelyek webhelyünkhöz kapcsolódnak,
a weboldal saját feltételei és szabályzatai alá tartozik.

## Változások az adatvédelmi irányelveinkben

Bármely változás, amit a jövőben az adatvédelmi irányelveinkhez adhatunk
ezen az oldalon és adott esetben e-mailben értesítjük Önt.
Győződjön meg róla, hogy gyakran látja a frissítéseket vagy módosításokat
Adatvédelmi irányelvek.

## Kapcsolatba lépni

Az adatvédelmi irányelvekkel kapcsolatos kérdések, észrevételek és kérelmek
üdvözölni, és címzettjei a [letsconnect@tipzone.net] címre (mailto:letsconnect@tipzone.net).
