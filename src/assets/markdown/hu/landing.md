## A TipZone-on különböző sportokban mérheted össze tippelési tudásod barátaiddal, munkatársaiddal vagy családtagjaiddal. Nem kell többet Excel táblázatban összeírni vagy papíron vezetni a tippeket. Nem kell számolgatni. Hozz létre csoportot, hívd meg, akit szeretnél, vagy csatlakozz már meglévő közösségekhez vagy partnereinkhez és mutasd meg, hogy te vagy a legjobb.

## A **TipZone** célja, hogy növeljük a közös szurkolás élményét.
## Az oldal használata teljesen ingyenes!

## Kérünk, vedd figyelembe, hogy az oldalt nem lehet használni nyereményszerzés céljából, így a csoport leírásban sem lehet pénznyereményt felajánlani a győztes(ek)nek!

# Tippelj és Szurkolj!
