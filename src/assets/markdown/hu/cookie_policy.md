# COOKIE-K (SÜTIK) KEZELÉSE:

Amikor Ön a tipzone.net oldalt olvassa, ez azzal is járhat, hogy cookie-kat vagy hasonló technológiákat használunk böngészőjének vagy eszközének azonosításához. A cookie egy kis fájl, amely akkor kerül az Ön számítógépére, amikor egy webhelyet keres fel. Amikor ismét felkeresi az adott webhelyet, a cookie-nak köszönhetően a webhely képes felismerni az Ön böngészőjét. 

A cookie-k tárolhatnak felhasználói beállításokat és egyéb információkat is. Más platformokon – ahol a cookie-k nem érhetők el vagy nem használhatók – egyéb technológiákat is használnak, amelyeknek a célja hasonlít a cookie-kéhoz: ilyen például a hirdetésazonosító az androidos mobileszközökön. 

Visszaállíthatja böngészője beállításait, hogy az utasítsa el az összes cookie-t, vagy hogy jelezze, ha a rendszer éppen egy cookie-t küld. 

Azonban lehet, hogy bizonyos webhelyfunkciók vagy szolgáltatások nem fognak megfelelően működni cookie-k nélkül.

A tipzone.net weboldalon a Google Analytics által használt cookie-k (sütik) működnek, melyek működésére a Google adatvédelmi irányelvei vonatkoznak (https://support.google.com/analytics/answer/6004245?hl=hu) .
