Ezen az oldalon azokat a kérdéseket találhatod, amelyek az oldal kezelése vagy a játék kapcsán merülhetnek fel. Amennyiben nem találtál a keresett kérdésedre választ, akkor azt elküldheted az support@tipzone.net email címre. 

### Hogy tudok saját csoportot létrehozni?
Csoportot a jobb felső sarokban található "csoportok kezelése" menüpontra kattintva hozhatsz létre. Az új csoport létrehozásánál csak egy csoportnévre és egy rövid leírásra van szükség. Ha meghívod a barátaidat a csoportba, akkor ezt a csoportnevet fogják majd látni a jobb oldali kezelőfelületen a továbbiakban.

### Hogy hívhatok meg valakit a csoportomba?
A csoportodba azt hívhatsz meg, akit szeretnél. Ehhez nem kell mást tenni, mint a kívánt csoportodon belül az admin felületen belül kattintani a játékosok kezelése menüpontra. Ezután csak az ismerőseid email címét kell beírni (egyenlőre erre csak egyesével van lehetőség). Ekkor, ha még nem regisztráltak akkor kapnak egy értesítő emailt, hogy lépjenek be a játékba és csatlakozzanak a csoportodhoz. Ha pedig már regisztráltak az oldalra, akkor azonnal megjelenik náluk a meghívásod.

### Tudok csoportot törölni?
Igen. A csoport létrehozásához hasonlóan szintén csak egy mozdulat az egész. A csoportok kezelése felületnél, azon belül a csoport módosításánál találod a csoport törlésére szolgáló gombot. Azonban, mielőtt kilépsz, valakit meg kell határozni csoportvezetőnek. Ha előtte mindenki kilép, vagy mindenkit kiraksz a csoportból, akkor a csoport megszűnik.

### Fogadhatok ugyanannak a mérkőzésnek más végeredményére a különböző csoportokban?
Nem, a mérkőzésekre csak egy végeredményt lehet leadni, amely az összes csoportnál ugyanúgy jelenik meg.

### Mi az a TipZone Ticket?
A **TipZone Ticket** a játékon belüli virtuálisan felhasználható élményfokozásra alkalmas fizetőeszköz. A TipZone Ticketek felhasználásával hozhatsz létre csoportot, illetve módosíthatod tippjeidet a mérkőzések kezdetéig.

### Hogyan juthatok TipZone Tickethez?
Minden játékos 15 TipZone ticketet kap a regisztráció után. 

Ezekhez három módok juthatsz hozzá:
* Ha egy mérkőzés eredményét pontosan eltaláltad, akkor 1 TT a jutalmad a 3 pont mellett
* Ha meghívod egy barátodat a TipZone-ba, akkor 3 TT a jutalmad
* Lehetőség van TT csomagok vásárlására az applikáción belül, így nem szükséges több meccset várnod, hogy létrehozz egy csoportot. 

### Hány csoportban lehetek tag?
Egy felhasználó bármennyi csoportban lehet tag, illetve hozhat létre csoportokat. Egy csoport létrehozása 10 TipZone Ticket. A kreditek felhasználására a szervereink kihasználtságának kordában tartása érdekében van szükség.

### Hogyan csatlakozhatok egy csoporthoz, hogyan lehetek egy csoport tagja?
Kétféleképpen lehetsz csoport tag. Az egyik esetben úgy tudsz csatlakozni egy csoporthoz, ha a csoport létrehozója (adminisztrátora) meghívott téged az email címed alapján. A másik esetben te hozol létre egy saját csoportot, ahova meghívod barátaidat az email címük megadásával. Ekkor létrehozhatsz csoportleírást, te találod ki a csoportnevet, illetve kezelheted a felhasználókat, és saját híreket írhatsz ki a többieknek (például egy közös meccsnézésre invitálhatod meg a többieket). 

### Megnézhetek-e olyan csoportállásokat, amelyekben nem vagyok benne?
Részben, mivel háromféle csoportot van lehetőség létrehozni. A nyilvános csoportokat, amelyekbe bárki beléphet, illetve azokat a zárt csoportokat, ahova maghívást kérhetsz, azokat megtekintheted. Azonban a zárt csoportokat nem láthatod, oda csak meghívással kerülhetsz be, így az ottani eredményeket és pontszámokat nem láthatod. Azonban minden játékos láthatja a globális táblázatot, ahol az összes felhasználó megjelenik.

### Tudok-e pénzt nyerni az oldal segítségével?
Nem, az oldalt nem lehet felhasználni haszonszerzés céljából. Az oldal csupán azoknak a társaságoknak segít, akik szeretnek egymás ellen játszani. Ezzel szeretnénk növelni egy-egy futballesemény szurkolói hangulatát.

### Mi alapján és mennyi pont jár a mérkőzésekért?
A játékban a mérkőzésekre való fogadások segítségével lehet pontokat szerezni. Egy mérkőzésen összesen maximum 3 pont szerezhető a várt eredmény megadásával. 1 pontot ér, amennyiben a mérkőzés végkimenetelét találja el a játékos (hazai csapat győzelme, vendég csapat győzelme, kivéve a döntetlen, mert arra más szabályok vonatkoznak) 1 pontot ér, ha a mérkőzés végkimenetele mellett a gólkülönbséget is eltalálja a játékos 2 pontot ér, ha a mérkőzés pontos végeredményét találja el a játékos

**Döntetlen szabály**: Mivel a sikeres döntetlen mérkőzéseknél történő tippelés során a gólkülönbséget biztosan eltalálja a játékos (ugye ez a szám 0) automatikusan 2 pontot kap, pontos végeredmény esetén pedig nem a gólkülönbséget figyeli a rendszer, így azért a végkimeneteli 1 pont mellé további 2 pontot ír jóvá a rendszer. Pontegyenlőség esetén a rendszer az alapján rangsorol, hogy mennyire pontosan találtad el a mérkőzéseket. Ugyanannyi pontnál a rendszer azt a játékost teszi előrébb a táblázaton, aki több mérkőzés végkimenetelét találta el pontosan, ha ez a szám megegyezik, akkor azok jönnek, akik a gólkülönbségeket találták el a mérkőzésen, végezetül pedig a mérkőzések végkimenetelét találták el.

### A fogadott meccseim tippjeit módosíthatom a mérkőzés kezdési időpontjáig?
Igen. A mérkőzések kezdő sípszójáig lehet a mérkőzésre módosítani a tippedet. Minden módosításhoz szükséged lesz a TipZone Ticketre. Egy mérkőzésre leadott tipp megváltoztatásáért 1 TipZone Ticketet kell adni.

### Hogyan regisztrálhatok?
A regisztráció egyszerű és gyors! Egy felhasználónevet kell választanod, megadni az email címedet és egy jelszót. Ez után kapsz kódot egy email ellenőrző levélben, amit a regisztráció véglegesítéséhez kell megadnod. Ha ez megvan, már kezdheted is a tippelést. Jó szórakozást!

### Mire használják az adataimat?
Az oldalon nem kérünk el semmilyen, úgynevezett érzékeny adatot (sensitive data). Csupán az email címet kérjük el. Ennek célja, ha esetleg elfelejted a jelszavad, akkor tudjunk emlékeztetőt küldeni. Az oldalon külön tudod kérni, mérkőzés emlékeztető hírlevelünket, amelynek segítségével kaphatsz értesítéseket a még meg nem tett mérkőzésekről. Ezt neked kell bekapcsolni regisztráció után, ha szeretnél ilyet kapni.

## Játékleírás
### Leírás
Az oldalon barátaiddal mérheted össze sport tudásod. A megtippelt mérkőzések után pontokat szerezhettek, ami után kialakul egy rangsor, így sport és bajnokság végén győztest hirdethettek magatok között. A csoportokon belül lehetősége van a csoport létrehozójának híreket írni, ahol közös meccsnézéseket szervezhettek, illetve a későbbiekben (és amennyiben igény lesz rá, egy külön chat felület segítségével vitathatjátok meg a mérkőzések végeredményeit)

### Menürendszer kezelése, az oldal használata
A belépés után a kezdőoldalra irányít az oldal, ahol a legfontosabb információként a következő két mérkőzést láthatod. Amennyiben még nem tetted meg a tipped a végeredményre, akkor figyelmeztet az oldal, így nem maradsz le róla, ha pedig már megtetted, akkor az általad beírt eredményt látod. Alatta három részre van osztva a képernyő. A bal oldali részen a csoportjaidat tudod áttekinteni. Itt az aktuális helyezésed és az első két játékost láthatod csoportonként. A középre eső területen egy hírfolyamot láthatsz, ahol az oldalon megjelenő utolsó három hírt láthatod. Hírenként egyet az oldal három hírterületéről (a három hírforrást a Hírek menüpont alatt találod). Jobb oldalt a csoportjaid híreit olvashatod el, így nem maradsz le semmilyen eseményről.

A **Hírek** menüpont alatt három féle hírrel találkozhatsz. Az első hasábon az oldallal kapcsolatos információkat találhatod meg. A második oszlopban az EB-vel kapcsolatos külső oldalakról érkező híreket olvashatsz, míg a harmadik részen a csoportjaid adminisztrátorai által (vagy saját csoport esetében a te) írt hírek jelennek meg.

**Tippjeim** menü alatt a mérkőzéseket láthatod. Az összes mérkőzés megjelenik 8-as csoportokra rendezve, hogy kényelmesen áttekinthető legyen. A már megtett mérkőzéseidnél láthatod a tipped, illetve ha már lejátszották a mérkőzést, akkor az azért kapott pontszámot is megjelenítjük. Azok a mérkőzések pedig amelyekre még nem tippeltél, azokra egy kiírás fog figyelmeztetni. A mérkőzések között egy lapozó gomb segítségével tudsz navigálni.

A **Csoportjaim** alatt azokat a csoportokat láthatod, amelyeknek tagja vagy, illetve a globális csoportot, amelyben az összes felhasználó szerepel, ez a TipZone.net saját csoportja. Itt teheted meg, hogy kilépsz egy csoportból.

A **GYIK**, azaz a Gyakran Ismétlődő Kérdéseknél olyan kérdéseket és válaszokat igyekeztünk összegyűjteni, amik az oldal használata során merülhetnek fel. Ha bármilyen kérdésed lenne nézz körül itt, amennyiben nem találtál választ akkor a <a href="mailto:support@tipzone.net">support@tipzone.net</a> email címen felteheted ezt a kérdést, és ha úgy látjuk akkor kitesszük azt az oldalra is.

A **Kapcsolat** menürésznél az oldallal kapcsolatos észrevételeidet vagy akár hirdetési megjelenési igényeidet jelezheted felénk.

Amennyiben egy játékostársad létrehoz egy csoportot és meghív oda, akkor a menürendszer jobb oldalán megjelenik egy Meghívásaim felirat, amire rákattintva eldöntheted, hogy szeretnél-e csatlakozni ahhoz a csoporthoz, vagy elutasíthatod a felkérést. 

