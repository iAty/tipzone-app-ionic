# Terms of Service

&nbsp;

  *The following terms and conditions (referred to herein as "**Terms of
Service**" or "**Agreement**") tells you the terms of use on which you
may make use of the [tipzone.net](https://www.tipzone.net) | website ("the Website") and all
content, services ("Service") and products available at or through the
website. The Website is owned and operated by TipZone Ltd. ("TipZone").
The Website is offered subject to your acceptance without modification
of all of the terms and conditions contained herein and all other
operating rules, policies (including, without limitation, TipZone's
Privacy Policy) and procedures that may be published from time to time
on this Website by TipZone.*

Please read this Agreement carefully before accessing or using the
Website as this Agreement will apply to your use of the Website. We
recommend that you print a copy of this for future reference. By
accessing or using any part of the Website or Service, you confirm that
you accept these Terms of Service and that you agree to comply with
them. If you do not agree to all the terms and conditions of this
Agreement, then you must not access the Website or use the Service.

TipZone reserves the right to update and change the Terms of Service
from time to time by amending this page without notice. Any new features
that augment or enhance the current Service, including the release of
new tools and resources, shall be subject to the Terms of Service.
Continued use of the Service after any such changes shall constitute
your consent to such changes. You can review the most current version of
the Terms of Service at any time at [https://www.tipzone.net/terms](https://www.tipzone.net/terms).
Please check this page from time to time to take notice of any changes
we make, as they are binding on you.

We may update our Website from time to time, and may change the content
at any time. However, please note that any of the content on our Website
may be out of date at any given time, and we are under no obligation to
update it.

We do not guarantee that our Website, or any content on it, will be free
from errors or omissions.

----

## Information About Us

We are TipZone Ltd, a company incorporated and registered in England and
Wales (company number: 9999999) with its registered office at [ADDRESS_HERE] (“**TipZone**”, the
“**Company**”, “**we**” or “**us**”).

## Account Terms

If you log in to the Website, you are responsible for maintaining the
security of your account and you are fully responsible for all
activities that occur under the account and any other actions taken in
connection with your account. You must treat the information in relation
to the security of your account, such as your user name, password or any
other piece of information as part of our security procedures as
confidential. You must not disclose it to any third party.

You must immediately notify TipZone if you know or suspect that anyone
other than you knows your username or password; if you become aware of,
or have reason to suspect any unauthorized uses of your account; or any
other breaches of security. TipZone will not be liable for any acts or
omissions by You, including any damages of any kind incurred as a result
of such acts or omissions.

Without limiting any of those representations or warranties, TipZone has
the right (though not the obligation) to, in TipZone's sole discretion
(i) refuse or remove any content that, in TipZone's opinion, violates
any TipZone policy or is in any way harmful or objectionable, or (ii)
terminate or deny access to and use of the Website to any individual or
entity for any reason. TipZone will have no obligation to provide a
refund of any amounts previously paid.

You also have to comply with the following Terms:

* You must be 13 years or older to use the Website and this Service.

* You must be a human. Accounts registered via automated methods are not
permitted.

* You must provide your valid email address, and any other information
requested in order to complete the signup process.

* You are responsible for maintaining the security of your account and
password. The Website cannot and will not be liable for any loss or
damage from your failure to comply with this security obligation.

* You are responsible for all activity that occurs under your account.

* You may not use the Website and Service for any illegal or unauthorized
purpose. You must not, in the use of the Website or Service, violate any
laws in your jurisdiction (including but not limited to copyright or
trademark laws).

We do not guarantee that our Website, or any content on it, will always
be available or be uninterrupted. Access to our Website is permitted on
a temporary basis. We may suspend, withdraw, discontinue or change all
or any part of our Website without notice. We will not be liable to you
if for any reason our Website is unavailable at any time or for any
period.

You are responsible for making all arrangements necessary for you to
have access to our Website. You are also responsible for ensuring that
all persons who access our Website through your internet connection are
aware of these terms of use and other applicable terms and conditions,
and that they comply with them.

----

## Intellectual Property

The Website and its original content, features and functionality are
owned by TipZone and are protected by international copyright,
trademark, patent, trade secret and other intellectual property or
proprietary rights laws. All such rights are reserved.

**We claim no intellectual property rights over the material you provide
to the Service. Your profile and materials uploaded or submitted remain
yours.** However, by setting your pages to be viewed publicly, you agree
to allow others to view your Content. You grant us a non-exclusive,
royalty free, perpetual licence of all intellectual property rights in
any material you provide for the purpose of our providing the Services
to you.

You may print off one copy, and may download extracts, of any page(s)
from our Website for your personal use and you may draw the attention of
others within your organisation to content posted on our Website.

You must not modify the paper or digital copies of any materials you
have printed off or downloaded in any way, and you must not use any
illustrations, photographs, video or audio sequences or any graphics
separately from any accompanying text.

Our status (and that of any identified contributors) as the authors of
content on our Website must always be acknowledged.

You must not use any part of the content on our Website for commercial
purposes without obtaining a licence to do so from us or our licensors.

If you print off, copy or download any part of our Website in breach of
these Terms of Service, your right to use our Website will cease
immediately and you must, at our option, return or destroy any copies of
the materials you have made.

----

## Payment and Renewal

Optional paid services and features are, or will be, available on the
Website (any such services, will be referred to as an "Upgrade"). By
selecting an Upgrade you agree to pay TipZone the monthly or annual
subscription fees indicated for that Upgrade. Payments will be charged
on a pre-pay basis on the day you sign up for an Upgrade and will cover
the use of that service for a monthly or annual subscription period as
indicated. Upgrade fees are not refundable.

A valid credit card is required for paying subscription fees .

The service is billed in advance on a monthly basis and is
non-refundable. There will be no refunds or credits for partial use of a
month’s service, upgrade/downgrade refunds, or refunds for months unused
with an active account.

Downgrading your account may cause loss of content, features or
capacity. TipZone does not accept liability for such loss.

## Automatic Renewal.

Unless you notify TipZone before the end of the applicable subscription
period that you want to cancel an Upgrade, your Upgrade subscription
will automatically renew and you authorize us to collect the
then-applicable annual or monthly subscription fee for such Upgrade (as
well as any taxes) using any credit card or other payment mechanism we
have on record for you. Upgrades can be cancelled at any time on the
**Account Settings** page.

----

## Modifications to the Service and Prices

TipZone reserves the right at any time and from time to time to modify
or discontinue, temporarily or permanently, the Service (or any part
thereof) with or without notice. Prices of all Services, including but
not limited to fees for the monthly subscription plan for the Service,
are subject to change upon 30 days’ notice from us. Such notice may be
provided at any time by posting the changes to the Website (tipzone.net)
or the Service itself. TipZone shall not be liable to you or to any
third party for any modification, price change, suspension or
discontinuance of the Service.

----

## Responsibility of Website Visitors

TipZone has not reviewed, and cannot review, all the material, including
computer software, posted to the Website, and cannot therefore be
responsible for that content, use or effects. By operating the Website,
TipZone does not represent or imply that it endorses the material there
posted, or that it believes such material to be accurate, useful or
non-harmful.

## Prohibited Use of the Website

You may use our Website only for lawful purposes. You may not use our
Website:

* in any way that breaches any applicable local, national or international
law or regulation;

* in any way that is unlawful or fraudulent, or has any unlawful or
fraudulent purpose or effect;

* for the purpose of harming or attempting to harm minors in any way;

* to send, knowingly receive, upload, download, use or re-use any material
which does not comply with our content standards.

* to transmit, or procure the sending of, any unsolicited or unauthorised
advertising or promotional material or any other form of similar
solicitation (spam);

* to knowingly transmit any data, send or upload any material that
contains viruses, Trojan horses, worms, time-bombs, keystroke loggers,
spyware, adware or any other harmful programs or similar computer code
designed to adversely affect the operation of any computer software or
hardware; or

* to mine cryptocurrency on the Website or any of the Services.

You also agree:

* not to reproduce, duplicate, copy or re-sell any part of our Website in
contravention of the provisions of these Terms of Service;

* Not to access without authority, interfere with, damage or disrupt:

    *  any part of our Website;

    * any equipment or network on which our Website is stored;

    * any software used in the provision of our Website; or

    * any equipment or network or software owned or used by any third party.

## Interactive Services

We may from time to time provide interactive services on our Website,
including, without limitation bulletin boards or comment threads
(“**interactive services**”).

We are under no obligation to oversee, monitor or moderate any
interactive service we provide on our Website, and we expressly exclude
our liability for any loss or damage arising from the use of any
interactive service by a user in contravention of our content standards,
whether the service is moderated or not.

## Content Standards

These content standards apply to any and all material which you
contribute to our Website (“**contributions**”), and to any interactive
services associated with it.

You must comply with the spirit and the letter of the following
standards. The standards apply to each part of any contribution as well
as to its whole.

Contributions must:

* be accurate (where they state facts);

* be genuinely held (where they state opinions); and

* comply with applicable law in the UK and in any country from which they
are posted.

Contributions must not:

* contain any material which is defamatory of any person;

* contain any material which is obscene, offensive, hateful or
inflammatory;

* promote sexually explicit material;

* promote violence;

* promote discrimination based on race, sex, religion, nationality,
disability, sexual orientation or age;

* infringe any copyright, database right, trade mark or any other right of
any other person;

be likely to deceive any person;

* be in breach of any legal duty owed to a third party, such as a
contractual duty or a duty of confidence;

* promote any illegal activity;

* be threatening, abuse or invade another’s privacy, or cause annoyance,
inconvenience or needless anxiety;

* be likely to harass, upset, embarrass, alarm or annoy any other person;

* be used to impersonate any person, or to misrepresent your identity or
affiliation with any person;

* give the impression that they emanate from us, if this is not the case;
or

* advocate, promote or assist any unlawful act such as (by way of example
only) copyright infringement or computer misuse.

## Suspension and Termination

We will determine, in our sole and absolute discretion, whether there
has been a breach of this prohibited use provision of these Terms of
Service through your use of our Website. When a breach of this provision
has occurred, we may take such action as we deem appropriate.

Failure to comply with this provision constitutes a material breach of
these Terms of Service upon which you are permitted to use our Website,
and may result in our taking all or any of the following actions:

* immediate, temporary or permanent withdrawal of your right to use our
Website;

* immediate, temporary or permanent removal of any posting or material
uploaded by you to our Website;

* issue of a warning to you;

* legal proceedings against you for reimbursement of all costs on an
indemnity basis (including, but not limited to, reasonable
administrative and legal costs) resulting from the breach;

* further legal action against you; and/or

* disclosure of such information to law enforcement authorities as we
reasonably feel is necessary.

We exclude liability for actions taken in response to breaches of this
provision of the Terms of Service. The responses described above are not
limited, and we may take any other action we reasonably deem
appropriate.

## Website Content

The content on our Website is provided for general information only. It
is not intended to amount to advice on which you should rely. You must
obtain professional or specialist advice before taking, or refraining
from, any action on the basis of the content on our Website.

We do not guarantee that our Website will be secure or free from bugs or
viruses. You are responsible for configuring your information
technology, computer programmes and platform in order to access our
Website. You should use your own virus protection software. You are
responsible for taking precautions as necessary to protect yourself and
your computer systems from viruses, worms, Trojan horses, and other
harmful or destructive content.

In addition, you must not misuse our Website by knowingly introducing
viruses, Trojans, worms, logic bombs or other material which is
malicious or technologically harmful. You must not attempt to gain
unauthorised access to our Website, the server on which our Website is
stored or any server, computer or database connected to our Website. You
must not attack our Website via a denial-of-service attack or a
distributed denial-of service attack. By breaching this provision, you
would commit a criminal offence. We will report any such breach to the
relevant law enforcement authorities and we will co-operate with those
authorities by disclosing your identity to them. In the event of such a
breach, your right to use our Website will cease immediately.

## Content Posted on Other Websites

We have not reviewed, and cannot review, all of the material, including
computer software, made available through the websites and webpages to
which the Website links, and that link to the Website. TipZone does not
have any control over those external resources, websites and webpages,
and is not responsible for their contents or their use. By linking to an
external website or webpage, TipZone does not represent or imply that it
endorses such website or webpage. You are responsible for taking
precautions as necessary to protect yourself and your computer systems
from viruses, worms, Trojan horses, and other harmful or destructive
content.

----

## Termination

TipZone may terminate your access to all or any part of the Website at
any time, with or without cause, with or without notice, effective
immediately. If you wish to terminate this Agreement or your account
registered on the Website (if you have one), you may simply discontinue
using the Website, however if you have signed up for subscription
services you will need to notify us that you wish to cancel your
subscription and delete your account on the Account Settings page. All
provisions of this Agreement which by their nature should survive
termination shall survive termination, including, without limitation,
ownership provisions, warranty disclaimers, indemnity and limitations of
liability.

----

## Disclaimer of Warranties

The Website is provided "as is". TipZone and its suppliers and licensors
hereby disclaim all warranties of any kind, express or implied,
including, without limitation, the warranties of merchantability,
fitness for a particular purpose and non-infringement. Neither TipZone
nor its suppliers and licensors, makes any warranty that the Website
will be error free or that access thereto will be continuous or
uninterrupted. You understand that you download from, or otherwise
obtain content or services through, the Website at your own discretion
and risk. Although we make reasonable efforts to update the information
on our Website, we make no representations, warranties or guarantees,
whether express or implied, that the content on our Website is accurate,
complete or up-to-date.

----

## Limitation of Liability

Nothing in these terms of use excludes or limits our liability for death
or personal injury arising from our negligence, or our fraud or
fraudulent misrepresentation, or any other liability that cannot be
excluded or limited by English law.

In no event will TipZone, or its suppliers or licensors, be liable with
respect to any subject matter of this agreement under any contract,
negligence, strict liability or other legal or equitable theory for: (i)
any special, incidental or consequential damages; (ii) the cost of
procurement for substitute products or services; (iii) for interruption
of use or loss or corruption of data; or (iv) for any amounts that
exceed the fees paid by you to TipZone under this agreement during the
twelve (12) month period prior to the cause of action. TipZone shall
have no liability for any failure or delay due to matters beyond their
reasonable control. The foregoing shall not apply to the extent
prohibited by applicable law.

----

## General Representation and Warranty

You represent and warrant that (i) your use of the Website will be in
strict accordance with the TipZone Privacy Policy, with this Agreement
and with all applicable laws and regulations (including without
limitation any local laws or regulations in your country, state, city,
or other governmental area, regarding online conduct and acceptable
content, and including all applicable laws regarding the transmission of
technical data exported from the United States or the country in which
you reside) and (ii) your use of the Website will not infringe or
misappropriate the intellectual property rights of any third party.

To the extent permitted by law, we exclude all conditions, warranties,
representations or other terms which may apply to our Website or any
content on it, whether express or implied.

----

## Indemnification

You agree to indemnify and hold harmless TipZone, its contractors, and
its licensors, and their respective directors, officers, employees and
agents from and against any and all claims and expenses, including
attorneys’ fees, arising out of your use of the Website, including but
not limited to your violation of this Agreement.

----

## Governing Law

Subject to overriding provisions of mandatory laws of the jurisdiction
of your domicile, this Agreement (and any further rules, policies, or
guidelines incorporated by reference), and any dispute or claim arising
out of or in connection with it or its subject matter or formation
(including non-contractual disputes or claims) shall be governed and
construed in accordance with the law of England and Wales.

Each party irrevocably agrees that the courts of England and Wales shall
have exclusive jurisdiction to settle any dispute or claim arising out
of or in connection with this agreement or its subject matter or
formation (including non-contractual disputes or claims). However, if
you are a consumer you may have the right to claim jurisdiction in the
country in which you reside.

----

## Contacting us

If you have any questions about this Agreement, please contact us at:

TipZone Ltd

[http://www.tipzone.net](http://www.tipzone.net)

TipZone Ltd. [ADDRESS_HERE]

00 00 00000 000

[letsconnect@tipzone.net](mailto:letsconnect@tipzone.net)


----

## Last modified

This Agreement was last modified on 19 March 2018
