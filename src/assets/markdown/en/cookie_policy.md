# Cookie policy

We use cookies and similar tools on our website,[http://www.tipzone.net/](http://www.tipzone.net/)
**(Website)** to improve its performance and enhance your user experience. This policy explains how we do that.

## What are cookies?

Cookies are small text files which a website may put on your computer or mobile device when you first visit a site or page. The cookie will help the website, or another website, to recognise your device the next time you visit.  We use the term “cookies” in this policy to refer to all files that collect information in this way.

There are many functions cookies serve. For example, they can help us to remember your username and preferences, analyse how well the Website is performing, or even allow us to recommend content we believe will be most relevant to you.

Certain cookies contain personal information – for example, if you click to “remember me” when logging in, a cookie will store your username. Most cookies will not collect information that identifies you, but will instead collect more general information such as how users arrive at and use the Website.

## What sort of cookies does the Website use?

Generally, our cookies perform up to four different functions:

1. *Essential cookies*

    Some cookies are essential for the operation of the Website. If you opt to disable these cookies, you will not be able to access or use all of the features that the Website incorporates.
2. *Performance Cookies*

    We utilise other cookies to analyse how our visitors use the Website and to monitor performance. This allows us to provide a high quality experience by customising our offering and quickly identifying and fixing any issues that arise. For example, we might use performance cookies to keep track of which pages are most popular, which method of linking between pages is most effective, and to determine why some pages are receiving error messages.

3. *Functionality Cookies*

    We use functionality cookies to allow us to remember your preferences. For example, cookies save you the trouble of typing in your username every time you use the Website, and recall your customisation preferences.
4. *Targeting Cookies*

    These cookies record your visit to our Website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose.]


## Does anyone else use cookies on the Website?

We do use or allow third parties to serve cookies that fall into the three categories above. For example, like many companies, we use Google Analytics to help us monitor our traffic. We may also use third party cookies to help us with market research, revenue tracking, improving site functionality and monitoring compliance with our terms and conditions and copyright policy

## Can you block cookies?

As we have explained above, cookies help you to get the most out of the Website.
However, if you do wish to disable cookies then you can do so by amending the settings within your browser or mobile device.
Please remember that if you do choose to disable cookies, you may find that certain sections of the Website do not work properly.

## More Information

More detail on how businesses use cookies is available at
[www.whatarecookies.com](http://www.whatarecookies.com/).
If you have any queries regarding this Cookie Policy please contact our Privacy Officer by e-mail at
[letsconnect@tipzone.net](mailto:letsconnect@tipzone.net)
