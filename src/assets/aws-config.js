// WARNING: DO NOT EDIT. This file is Auto-Generated by AWS Mobile Hub. It will be overwritten.

// Copyright 2017-2018 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to
// copy, distribute and modify it.

// AWS Mobile Hub Project Constants
var aws_app_analytics = 'enable';
var aws_auth_facebook = 'enable';
var aws_cloud_logic = 'enable';
var aws_cloud_logic_custom = [{"id":"i2i47euvdj","name":"group-newsCRUD","description":"","endpoint":"https://i2i47euvdj.execute-api.eu-west-2.amazonaws.com/Development","region":"eu-west-2","paths":["/group-news","/group-news/123"]},{"id":"9n7i9a3ywg","name":"resultsCRUD","description":"","endpoint":"https://9n7i9a3ywg.execute-api.eu-west-2.amazonaws.com/Development","region":"eu-west-2","paths":["/results","/results/123"]},{"id":"fklo3yrdh1","name":"sport-groupsCRUD","description":"","endpoint":"https://fklo3yrdh1.execute-api.eu-west-2.amazonaws.com/Development","region":"eu-west-2","paths":["/sport-groups","/sport-groups/123"]},{"id":"bkkrbc0ub0","name":"user-resultsCRUD","description":"","endpoint":"https://bkkrbc0ub0.execute-api.eu-west-2.amazonaws.com/Development","region":"eu-west-2","paths":["/user-results","/user-results/123"]},{"id":"4skjk0ojyf","name":"invitation","description":"","endpoint":"https://4skjk0ojyf.execute-api.eu-west-2.amazonaws.com/Development","region":"eu-west-2","paths":["/send-invitation","/send-invitation/123"]},{"id":"g5wasff0yi","name":"test user config","description":"","endpoint":"https://g5wasff0yi.execute-api.eu-west-2.amazonaws.com/Development","region":"eu-west-2","paths":["/get-user-config","/get-user-config/123"]}];
var aws_cognito_identity_pool_id = 'eu-west-2:1d367ab2-2626-4f2f-919b-b0b37731c284';
var aws_cognito_region = 'eu-west-2';
var aws_content_delivery = 'enable';
var aws_content_delivery_bucket = 'tipzone-hosting-mobilehub-1836564323';
var aws_content_delivery_bucket_region = 'eu-west-2';
var aws_content_delivery_cloudfront = 'enable';
var aws_content_delivery_cloudfront_domain = 'd34qb6pubgrs2a.cloudfront.net';
var aws_dynamodb = 'enable';
var aws_dynamodb_all_tables_region = 'eu-west-2';
var aws_dynamodb_table_schemas = [{"tableName":"tipzone-mobilehub-1836564323-invitations","attributes":[{"name":"inviteId","type":"S"},{"name":"userEmail","type":"S"},{"name":"createdAt","type":"S"},{"name":"invitationFrom","type":"S"}],"indexes":[],"region":"eu-west-2","hashKey":"inviteId","rangeKey":"userEmail"},{"tableName":"tipzone-mobilehub-1836564323-LoginTrail","attributes":[{"name":"userId","type":"S"},{"name":"activityDate","type":"S"}],"indexes":[{"indexName":"UserId-ActivityDate-Index","hashKey":"userId","rangeKey":"activityDate"}],"region":"eu-west-2","hashKey":"userId","rangeKey":"activityDate"},{"tableName":"tipzone-mobilehub-1836564323-sport-groups","attributes":[{"name":"groupId","type":"S"},{"name":"updatedAt","type":"N"},{"name":"createdAt","type":"N"},{"name":"featured","type":"BOOL"}],"indexes":[],"region":"eu-west-2","hashKey":"groupId","rangeKey":"updatedAt"},{"tableName":"tipzone-mobilehub-1836564323-results","attributes":[{"name":"resultId","type":"S"}],"indexes":[],"region":"eu-west-2","hashKey":"resultId"},{"tableName":"tipzone-mobilehub-1836564323-user-results","attributes":[{"name":"resultId","type":"S"},{"name":"userId","type":"S"},{"name":"away","type":"M"},{"name":"createdAt","type":"S"},{"name":"home","type":"M"},{"name":"pickCount","type":"N"},{"name":"seasonId","type":"S"},{"name":"updatedAt","type":"S"},{"name":"userScore","type":"N"}],"indexes":[{"indexName":"resultId-userId-index","hashKey":"resultId","rangeKey":"userId"}],"region":"eu-west-2","hashKey":"resultId","rangeKey":"userId"},{"tableName":"tipzone-mobilehub-1836564323-user-custom-config","attributes":[{"name":"userId","type":"S"}],"indexes":[],"region":"eu-west-2","hashKey":"userId"},{"tableName":"tipzone-mobilehub-1836564323-standings","attributes":[{"name":"seasonId","type":"S"},{"name":"rank","type":"N"},{"name":"createdAt","type":"N"},{"name":"current_outcome","type":"S"},{"name":"draw","type":"N"},{"name":"goal_diff","type":"N"},{"name":"goals_against","type":"N"},{"name":"goals_for","type":"N"},{"name":"loss","type":"N"},{"name":"played","type":"N"},{"name":"points","type":"N"},{"name":"team","type":"M"},{"name":"updatedAt","type":"N"},{"name":"win","type":"N"}],"indexes":[],"region":"eu-west-2","hashKey":"seasonId","rangeKey":"rank"},{"tableName":"tipzone-mobilehub-1836564323-group-news","attributes":[{"name":"groupId","type":"S"},{"name":"updateAt","type":"N"},{"name":"content","type":"S"},{"name":"title","type":"S"},{"name":"userId","type":"S"}],"indexes":[],"region":"eu-west-2","hashKey":"groupId","rangeKey":"updateAt"}];
var aws_facebook_app_id = '320547508452752';
var aws_facebook_app_permissions = 'public_profile';
var aws_google_app_permissions = 'email,profile,openid';
var aws_google_web_app_id = '789308346375-u6ap7efm1euvf4ro2m2c3a1e6e43ri0o.apps.googleusercontent.com';
var aws_mobile_analytics_app_id = 'ba6bc0e87d4742828c5173e78e16e97c';
var aws_mobile_analytics_app_region = 'us-east-1';
var aws_project_id = 'bf262a9f-09ed-424e-88e7-18dfa323dce4';
var aws_project_name = 'TipZone';
var aws_project_region = 'eu-west-2';
var aws_push_pinpoint = 'enable';
var aws_resource_bucket_name = 'tipzone-deployments-mobilehub-1836564323';
var aws_resource_name_prefix = 'tipzone-mobilehub-1836564323';
var aws_sign_in_enabled = 'enable';
var aws_user_files = 'enable';
var aws_user_files_s3_bucket = 'tipzone-userfiles-mobilehub-1836564323';
var aws_user_files_s3_bucket_region = 'eu-west-2';
var aws_user_pools = 'enable';
var aws_user_pools_id = 'eu-west-2_LQo9X82RC';
var aws_user_pools_mfa_type = 'OFF';
var aws_user_pools_web_client_id = '6vrajo4gjm4kddvutt161rttob';

AWS.config.region = aws_project_region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: aws_cognito_identity_pool_id
  }, {
    region: aws_cognito_region
  });
AWS.config.update({customUserAgent: 'MobileHub v0.1'});
