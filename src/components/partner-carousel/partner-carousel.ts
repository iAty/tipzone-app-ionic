import { Component } from '@angular/core';

/**
 * Generated class for the PartnerCarouselComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'partner-carousel',
  templateUrl: 'partner-carousel.html'
})
export class PartnerCarouselComponent {

  text: string;
  partners: any;

  constructor() {
    this.text = 'Hello World';
    this.partners = [
      {
        name: 'Index.hu',
        image: '/assets/partners/index_hu.png'
      },
      {
        name: 'Lavard',
        image: '/assets/partners/lavard-light.png'
      },
      {
        name: 'Heineken',
        image: '/assets/partners/heineken.png'
      },
      {
        name: 'Digi TV',
        image: '/assets/partners/digitv.png'
      }
    ]
  }

}





