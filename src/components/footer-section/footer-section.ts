import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the FooterSectionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'footer-section',
  templateUrl: 'footer-section.html'
})
export class FooterSectionComponent {
  public links = [
    { name: 'Impressum', path: 'impressum' },
    { name: 'Terms & Conditions', path: 'terms_of_use' },
    { name: 'Cookie policy', path: 'cookie_policy' },
    { name: 'Privacy & Policy', path: 'privacy_and_policy' },
    { name: 'FAQ', path: 'faq' },
  ];

  constructor(private navCtrl: NavController, translate: TranslateService) {
    translate.get(['IMPRESSUM', 'TERMS_AND_CONDITIONS', 'COOKIE_POLICY', 'PRIVACY_AND_POLICY', 'FAQ']).subscribe( translations => {
      this.links[0].name = translations['IMPRESSUM'];
      this.links[1].name = translations['TERMS_AND_CONDITIONS'];
      this.links[2].name = translations['COOKIE_POLICY'];
      this.links[3].name = translations['PRIVACY_AND_POLICY'];
      this.links[4].name = translations['FAQ'];
    })
  }

  staticPage(staticPage: string): void {
    this.navCtrl.setRoot('static-page', { 'pageId': staticPage });
  }

}

