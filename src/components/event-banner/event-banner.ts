import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DatabaseService } from '../../providers/db';
import { AlertController } from 'ionic-angular';
import { LogEntryService } from '../../providers/log-events';
/**
 * Generated class for the EventBannerComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */


export interface Event {
  competitors: string;
  abs: string;
  flag: string;
  points: number;
  pick?: number;
  done: boolean;
}

export enum GameStatus {
  isStarted = 'is-stared',
  notStarted = 'not-stared',
  isActive = 'is-active',
  isEnded = 'is-ended',
}

@Component({
  selector: 'event-banner',
  templateUrl: 'event-banner.html'
})
export class EventBannerComponent implements OnInit {
  @Input('result') result: any;
  @Input('userpick') userpick: any;

  homeTeam: any;
  guestTeam: any;
  text: string;
  pickOne = false;
  pickTwo = false;
  doneEditing: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  pickerTime = null;

  public date: any;
  public eventType: any;
  public isActive = false;
  public isStarted = false;
  public isEnded = false;

  constructor(
    private db: DatabaseService,
    private alertCtrl: AlertController,
    private logService: LogEntryService) {

   }


  ngOnInit() {
    this.date = this.result.scheduled;
    this.homeTeam = this.result.competitors[0] || this.userpick.home;
    this.guestTeam = this.result.competitors[1] || this.userpick.away;

    if (this.result.sport_event_status && this.result.sport_event_status.status === 'closed') {
      this.doneEditing.next('done');
      this.isEnded = true;
    }


      if (this.userpick && this.userpick.locked && this.userpick.home.points && this.userpick.away.points) {
        this.doneEditing.next('done');
        this.homeTeam.pick = this.userpick.home.points;
        this.guestTeam.pick = this.userpick.away.points;
      }


  }

  isFinished(): boolean {
    return this.result.sport_event_status.match_status === 'end';
  }

  addPoints(team: any) {
    team.done = true;
  }

  clearBets(): void {
    if (this.isEnded) {
      let alert = this.alertCtrl.create({
        title: 'This match ended',
        subTitle: 'Sorry you could not change your result after thr match finished!',
        buttons: ['Dismiss']
      });
      alert.present();
    } else if (this.result.status !== 'not_stared') {
      let alert = this.alertCtrl.create({
        title: 'Thi pick period finished',
        subTitle: 'Sorry you could not change your result now!',
        buttons: ['Dismiss']
      });
      alert.present();
    } else {
      this.homeTeam.pick = null;
      this.guestTeam.pick = null;
      this.doneEditing.next(null);
      this.db.setUserPickByResultId({
        resultId: this.result.resultId,
        seasonId: this.result.seasonId,
        home: {
          competitorId: this.homeTeam.id,
          points: this.homeTeam.pick
        },
        away: {
          competitorId: this.guestTeam.id,
          points: this.guestTeam.pick
        },
        createdAt: new Date().toString(),
        updatedAt: new Date().toString()
      }, false)
      .then(
        successPick => {
          // console.log('pick was removed success');
          this.logService.writeLogEntry('REMOVED_PICK', {resultId: this.result.resultId});
        }
      )
      .catch(
        hasFailed => console.warn('pick remove has failed', hasFailed)
      );
    }
  }

  dropDown(key: string): void {
    // console.log('key', key);
    this.pickOne = (key === 'pickone')
    this.pickTwo = (key === 'picktwo')
  }

  pickPoint(key: string, pick: any): void {
    // console.log('pick', key, pick);

    if (key === 'pick1') {
      this.homeTeam.pick = pick;
      this.pickOne = false;
    }
    if (key === 'pick2') {
      this.guestTeam.pick = pick;
      this.pickTwo = false;
    }

  }

  hasPoints(): boolean {
    return (this.homeTeam.points && this.guestTeam.points) || (this.homeTeam.pick && this.guestTeam.pick);
  }

  dismiss(): void {
    this.pickOne = false;
    this.pickTwo = false;
  }

  saveValue(): void {
    if (this.homeTeam.pick && this.guestTeam.pick && !this.isEnded) {
        this.doneEditing.next('done');
        this.db.setUserPickByResultId({
          resultId: this.result.resultId,
          seasonId: this.result.seasonId,
          home: {
            competitorId: this.homeTeam.id,
            points: this.homeTeam.pick
          },
          away: {
            competitorId: this.guestTeam.id,
            points: this.guestTeam.pick
          },
          createdAt: new Date().toString(),
          updatedAt: new Date().toString()
        })
        .then(
          successPick => {
            // console.log('pick was success');
            this.logService.writeLogEntry('PICK', {resultId: this.result.resultId});
          }
        )
        .catch(
          hasFailed => console.warn('pick has failed', hasFailed)
        );
    }
  }

  getCompetitorFlag(result: any): string | null {
    if (!result) { return null; }
    let competitorId = result.id.split(":")[2];
    return '/assets/flags/' + competitorId  + '.png';
  }

}
