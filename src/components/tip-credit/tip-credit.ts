import { Component, OnInit } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from './popover-page';
import { AuthenticationService } from '../../providers/user';
import { UserStore } from '../../stores/user.store';
/**
 * Generated class for the TipCreditComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tip-credit',
  templateUrl: 'tip-credit.html'
})
export class TipCreditComponent implements OnInit {

  text: number;

  constructor(public popoverCtrl: PopoverController, public user: AuthenticationService, public userStore: UserStore) {
    this.text = 0;

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.userStore.userConfig.subscribe( (userConfig:any) => {
      this.text = (userConfig) ? userConfig.credit : 0;
      });

  }

  creditPopover(ev: UIEvent): void {
    let popover = this.popoverCtrl.create(PopoverPage, {
      credit: this.text
    });
    popover.present({
      ev: ev
    });
  }

}



