
import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'popover-page',
  template: `
  <ion-content>
    <ion-list>
      <ion-list-header>My Tickets: ({{MyCredit | number:'4.0-0'}})</ion-list-header>
      <button ion-item (click)="close()">Buy more</button>
      <button ion-item (click)="close('faq_credit')">What is it?</button>
      <button ion-item (click)="close()">My Activity</button>
    </ion-list>
  </ion-content>
  `
})
export class PopoverPage {
  MyCredit: number;

  constructor(public viewCtrl: ViewController, private navParams: NavParams) { }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    if (this.navParams.data) {
      this.MyCredit = this.navParams.data.credit;
    }
  }
  close(target?: string) {
    this.viewCtrl.dismiss(target);
  }
}




