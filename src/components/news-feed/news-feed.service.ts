import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import 'rxjs/add/operator/share'
import { List } from 'immutable'
import * as _orderBy from 'lodash.orderby'

const NEWS_FEED_API = environment.NEWS_FEED_API;
const NEWS_FEED_API_KEY = environment.NEWS_FEED_API_KEY;

export interface NewsArticles {
  source: {
    id: string;
    name: string;
  },
  author: string;
  title: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
}

export interface NewsFeedResponse {
  status: string;
  totalResults: number;
  articles: NewsArticles[]
}

@Injectable()
export class NewsFeedService {
  _news: BehaviorSubject<any> = new BehaviorSubject<any>(<any>[]);
  constructor(private httpClient: HttpClient) { }

  get news() { return Observable.create(fn => this._news.subscribe(fn)) }

  refresh(): Observable<any> {
    let observable = this.getNews().share()
    observable.subscribe(resp => this._news.next(List(this.sort(resp.articles))))
    return observable;
  }

  getNews(): Observable<any> {
    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json');
    let httpParams = new HttpParams()
    .set('country', 'hu')
    .set('category', 'sports')
    .set('pageSize', '50')
    .set('apiKey', NEWS_FEED_API_KEY);
    let options = {
      headers: httpHeaders,
      params: httpParams
    }
    return this.httpClient.get<NewsFeedResponse>(NEWS_FEED_API, options);
  }

  private sort(news: any[]): any[] {
    return _orderBy(news, ['publishedAt'], ['asc'])
  }

}
