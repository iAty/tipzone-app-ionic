import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { NewsFeedService, NewsArticles } from './news-feed.service';

/**
 * Generated class for the NewsFeedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

 export interface Feed {
    fid: number;
    title: string;
    description: string;
    timestamp: number;
    type: string;
 }

@Component({
  selector: 'news-feed',
  templateUrl: 'news-feed.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsFeedComponent implements OnInit {
  feedData: any;
  showLimit = 4;
  newsLimits = 10;
  hasMore = true;
  @Input() noavatar = false;
  @Input() feed;

  constructor(
    private navCtrl: NavController,
    public newsService: NewsFeedService,
    public modalCtrl: ModalController
  ) {
    
  }

  prettifyUrl(title: string): string {
    return title.toLowerCase().replace(/[^a-zA-Z0-9]/g,'_');
  }

  clickNews(newsId: any): void {
    let profileModal = this.modalCtrl.create('NewsPage', { newsId: newsId });
    profileModal.present();
  }

  ngOnInit() {
    if (!this.feed) {
      this.newsService.refresh().subscribe( feeds => this.feedData = feeds, hasError => console.warn(hasError));
    } else {
      this.feedData = this.feed;
    }
  }




  showMore(): void {
    this.showLimit += +this.newsLimits;
  }

  get newssize() { return this.newsService.news.map((news) => news.size) }
}
