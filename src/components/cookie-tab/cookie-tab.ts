import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { CookieService, CookieOptions } from 'ngx-cookie';

@Component({
  selector: 'tipzone-cookie-tab',
  templateUrl: 'cookie-tab.html'
})
export class CookieTabComponent implements OnInit {
  hasCookie = false;
  hideCookieTab = false;
  constructor(
    private translate: TranslateService,
    @Inject(DOCUMENT) private document: any,
    @Inject(PLATFORM_ID) private platformId: any,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.hasCookie = !!this.cookieService.get('cookie_policy');
    }
  }

  acceptCookiePolicy(event: Event): void {
    if (isPlatformBrowser(this.platformId)) {
      // const expireDate: string = new Date(new Date().getDate() - 1).toUTCString();
      // const cookieOptions: CookieOptions = {
      //   expires: expireDate,
      //   secure: false,
      //   httpOnly: false,
      //   storeUnencoded: true
      // };
      this.cookieService.put('cookie_policy', 'accepted');
      this.hideCookieTab = true;
      setTimeout( () => {
        this.hasCookie = true;
      }, 580);
    }
  }

}
