import { NgModule } from '@angular/core';
import { NewsFeedComponent } from './news-feed/news-feed';
import { EventBannerComponent } from './event-banner/event-banner';
import { HomeSectionComponent } from './home-section/home-section';
import { TopNavigationComponent } from './top-navigation/top-navigation';
import { IonicModule } from 'ionic-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FooterSectionComponent } from './footer-section/footer-section';
import { LeaderBoardComponent } from './leader-board/leader-board';
import { SeasonStandingComponent } from './season-standing/season-standing';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../pipes/pipes.module';
import { GroupCardComponent } from './group-card/group-card';
import { PartnerCarouselComponent } from './partner-carousel/partner-carousel';
import { CookieTabComponent } from './cookie-tab/cookie-tab';
import { LoginFormComponent } from './login-form/login-form';
import { RegisterFormComponent } from './register-form/register-form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TipCreditComponent } from './tip-credit/tip-credit';
import { SportCardComponent } from './sport-card/sport-card';
import {NgPipesModule} from 'ngx-pipes';

const COMPONENTS = [
  NewsFeedComponent,
  EventBannerComponent,
  HomeSectionComponent,
  TopNavigationComponent,
  FooterSectionComponent,
  LeaderBoardComponent,
  SeasonStandingComponent,
  GroupCardComponent,
  PartnerCarouselComponent,
  LoginFormComponent,
  RegisterFormComponent,
  TipCreditComponent,
  SportCardComponent
];


@NgModule({
	declarations: [
    ...COMPONENTS,
  ],
	entryComponents: [
    ...COMPONENTS
  ],
  imports: [
    FlexLayoutModule,
    PipesModule,
    NgPipesModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    IonicModule
  ],
  exports: [
    ...COMPONENTS,
  ],
  providers: []
})
export class ComponentsModule {}
