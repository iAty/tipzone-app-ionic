import { Component, EventEmitter, Output } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';
import { AuthenticationService } from '../../providers/user';
import { Storage } from '@ionic/storage';
import { NavigationContainer } from 'ionic-angular/navigation/navigation-container';
import { LoginDetails } from '../../models/forms.model';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the LoginFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'login-form',
  templateUrl: 'login-form.html'
})
export class LoginFormComponent {
  @Output() reset = new EventEmitter<boolean>();
  public loginDetails: LoginDetails;
  text: string;
  error: any;

  constructor(
    private loadingCtrl: LoadingController,
    private user: AuthenticationService,
    private storage: Storage,
    public translate: TranslateService,
    private navCtrl: NavController
  ) {
    this.loginDetails = new LoginDetails();
  }

  resetPassword(e: Event): void {
    e.preventDefault();
    this.reset.emit(true);
  }


  login() {
    let loading = this.loadingCtrl.create({
      content: this.translate.instant('PLEASE_WAIT')
    });
    loading.present();

    let details = this.loginDetails;

    this.user.login(details).then((result) => {

      loading.dismiss();
      this.user.goToLogin().then( navigateTo => {
        if (navigateTo) {
          this.navCtrl.setRoot(navigateTo);
        }
      }).catch( hasError => console.error(hasError));

    }).catch((err) => {
      if (err.message === "User is not confirmed.") {
        loading.dismiss();
        this.navCtrl.push('ConfirmPage', { 'username': details.username });
      }
      this.error = err;
      loading.dismiss();
    });
  }

}
