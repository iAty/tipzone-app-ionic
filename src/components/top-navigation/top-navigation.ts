import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { AuthenticationService } from '../../providers/user';
import { TranslateService } from '@ngx-translate/core';
import { I18nSwitcherProvider } from '../../providers/i18n-switcher';
import { ThemeSwitcherProvider } from '../../providers/theme-switcher';


/**
 * Generated class for the TopNavigationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'top-navigation',
  templateUrl: 'top-navigation.html'
})
export class TopNavigationComponent implements OnInit {
  isSearch = false;

  theme = 'light';
  userAvatar: any;

  constructor(
    private auth: AuthenticationService,
    public translate: TranslateService,
    public menuCtrl: MenuController,
    private themeSwitcherProvider: ThemeSwitcherProvider,
    public navCtrl: NavController) {
       
    }

    ngOnInit(): void {
      //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
      //Add 'implements OnInit' to the class.
      // this.auth.isAuthenticated().then( isAuth => {
        
       if (this.auth.isUserSignedIn()) {
          // // console.log('top nav user data:');
          this.auth.refreshAvatar().then( userAvatar => {
            const img = new Image();
            const self = this;
            img.onload = function(success) {
              console.log(success);
              self.userAvatar = userAvatar;
            }
            img.src = userAvatar;
            
            }).catch( hasError => console.warn('auth error ', hasError));
        } 
      // })
    }

  goHome(e: Event): void {
    e.preventDefault();

    let toHome = (this.auth.isUserSignedIn()) ? 'tabs' : 'login-page';
    this.navCtrl.setRoot(toHome);
  }

  accountPage(): void {
    this.navCtrl.setRoot('account-page');
  }

  loginModal(event: Event): void {
    event.preventDefault();
    this.navCtrl.setRoot('login-page');
  }



  themeSwitch() {
    this.theme = (this.theme === 'heineken') ? 'rockys' : 'heineken';
    this.themeSwitcherProvider.switchColor(this.theme);
  }

  toggleMenu() {
    this.menuCtrl.toggle();
  }
}


