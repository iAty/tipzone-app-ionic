import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, LoadingController } from 'ionic-angular';
import * as UUID from 'uuid';
import { DynamoDB } from '../../providers/providers';
import { DatabaseService } from '../../providers/db';
import { ResultsStore } from '../../stores/results.store';


declare var AWS: any;

/**
 * Generated class for the HomeSectionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'home-section',
  templateUrl: 'home-section.html'
})
export class HomeSectionComponent implements OnInit {

  text: string;
  userDetails = {
    "sr:season:48238": {
      name: 'Világbajnokság',
      position: 1,
      sportName: 'Futball',
      points: 0
    },
    // "sr:season:41778": {
    //   name: 'Series A',
    //   position: 3,
    //   sportName: 'Soccer',
    //   points: 1234
    // },
    // "sr:season:41779": {
    //   name: 'WCDivision I. Group A',
    //   sportName: 'Ice Hockey',
    //   position: 1234,
    //   points: 44
    // },
    // "sr:season:41781": {
    //   name: 'Series A',
    //   position: 1,
    //   sportName: 'Soccer',
    //   points: 1234
    // },
    // "sr:season:41782": {
    //   name: 'WCDivision I. Group A',
    //   sportName: 'Ice Hockey',
    //   position: 4,
    //   points: 44
    // },
    // "sr:season:41783": {
    //   name: 'WCDivision I. Group A',
    //   sportName: 'Ice Hockey',
    //   position: 4,
    //   points: 44
    // }
  };

  constructor(
    private navCtl: NavController,
    private modalCtrl: ModalController,
    private ddb: DatabaseService,
    public loadingCtrl: LoadingController,
    public resultStore: ResultsStore) {
    this.text = 'Hello World';
  }
  ngOnInit(): void {
    this.resultStore.userStanding();
    this.ddb.getUsersStanding().then( results => {
      
          for (let season of Object.keys(results)) {
            for (let scores of Object.keys(results[season])) {
              console.log('[USER result]:', results[season][scores]);
              if (!this.userDetails[season]) {
                this.userDetails[season] = {};
              }

              if (this.userDetails[season].points === undefined) {
                this.userDetails[season] = {
                  points: 0
                }
              }
              
            
              if (results[season][scores] && results[season][scores].userScore !== undefined) {
                this.userDetails[season].points += +results[season][scores].userScore;
                console.log(this.userDetails[season].points, results[season][scores].userScore);
              }
            }
          }
      });
    // this.db.getUsersStanding().then( results => {

   
    // })
  }

  createGroup(): void {
    let newGroupModal = this.modalCtrl.create('GroupCreatePage', { groupId: UUID()  });
    newGroupModal.onDidDismiss(groupData => {
      if (groupData) {
        // console.log('Group Done', groupData);
        this.navCtl.push('GroupPage', {
          groupId: groupData,
          created: 'welcome'
        });
      };
    });
    newGroupModal.present();
  }

  searchGroup(): void {
    this.navCtl.parent.select(2);
  }
}



