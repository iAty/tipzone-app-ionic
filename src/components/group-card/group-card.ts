import { Component, Input, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the GroupCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

declare var AWS: any;
declare const aws_user_files_s3_bucket_region;
declare const aws_user_files_s3_bucket;

@Component({
  selector: 'group-card',
  templateUrl: 'group-card.html'
})
export class GroupCardComponent implements OnInit {
  @Input() group: any;
  private s3: any;
  public avatarPhoto: string;
  public ownerPhoto: string;

  constructor(private navCtrl: NavController) {
    this.s3 = new AWS.S3({
      'params': {
        'Bucket': aws_user_files_s3_bucket
      },
      'region': aws_user_files_s3_bucket_region
    });

  }

  ngOnInit() {

    if (this.group.hasCover) {
       this.s3.getSignedUrl('getObject', { 'Key': 'protected/group/' + this.group.groupId + '/cover.jpg' }, (err, url) => {
        this.avatarPhoto = url;
      });
    }

    if (this.group.userId) {
       this.s3.getSignedUrl('getObject', { 'Key': 'protected/user/' + this.group.userId + '/avatar.jpg' }, (err, url) => {
        this.ownerPhoto = url;
      });
    }
  }

  selectItem(item: any): void {
    if (!item.closed) {
        this.navCtrl.push('GroupPage', {
          groupId: item.groupId
        });
    }
  }
}


