import { Component, Input } from '@angular/core';

/**
 * Generated class for the SportCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sport-card',
  templateUrl: 'sport-card.html'
})
export class SportCardComponent {
  @Input() sport: any;
  

  constructor() {
    
  }

  navigateToGroups(ev: UIEvent, sportId: any): void {
    /// navigate to Sport by Id
  }
}
