import { Component, OnInit, Input } from '@angular/core';
import { PopoverController, NavParams } from 'ionic-angular';
import { LeaderPopoverPage } from './leader-popover-page';
/**
 * Generated class for the LeaderBoardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

 export interface TabellaScores {
   userName: string;
   match: number;
   winner: number;
   difference: number;
   points: number;
   results: Array<{
     home: {
       competitor: string;
       score: number;
       pick: number;
     }
     away: {
       competitor: string;
       score: number;
       pick: number;
     }
     userScore: number;
   }>
 }


@Component({
  selector: 'leader-board',
  templateUrl: 'leader-board.html'
})
export class LeaderBoardComponent implements OnInit {
  @Input() seasonId: any;
  @Input() isMember = false;
  @Input() standalone = false;
  userDetails: Array<TabellaScores>;

  constructor(private popoverCtrl: PopoverController) {

  }

  presentPopover(ev, results) {

    let popover = this.popoverCtrl.create(LeaderPopoverPage, {
      results: results
    });

    popover.present({
      ev: ev
    });
  }




  ngOnInit(): void {
    this.userDetails = [
      {
        userName: 'Bela',
        match: 3,
        winner: 5,
        difference: 0,
        points: 123,
        results: [
          {
            home: {
              competitor: 'Russia',
              score: 2,
              pick: 2
            },
            away: {
              competitor: 'Costa Rica',
              score: 0,
              pick: 0,
            },
            userScore: 3
          },
          {
            home: {
              competitor: 'Germany',
              score: 3,
              pick: 2
            },
            away: {
              competitor: 'Croatia',
              score: 1,
              pick: 0,
            },
            userScore: 2
          }
        ]
      }
    ]
    //  this.ddb.getUsersStanding().then( results => {
      
    //       for (let season of Object.keys(results)) {
    //         for (let scores of Object.keys(results[season])) {
    //           console.log('[All result]:', results[season][scores]);
    //           if (!this.userDetails[season]) {
    //             this.userDetails[season] = {};
    //           }

    //           if (this.userDetails[season].points === undefined) {
    //             this.userDetails[season] = {
    //               points: 0
    //             }
    //           }
              
            
    //           if (results[season][scores] && results[season][scores].userScore !== undefined) {
    //             this.userDetails[season].points += +results[season][scores].userScore;
    //             console.log(this.userDetails[season].points, results[season][scores].userScore);
    //           }
    //         }
    //       }
    //   });
    
  }

}


