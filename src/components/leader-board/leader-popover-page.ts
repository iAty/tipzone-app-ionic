import { Component, OnInit, Input } from '@angular/core';
import { PopoverController, NavParams } from 'ionic-angular';

@Component({
  template: `
  <ion-list>
    <ion-item *ngFor="let result of results">
    {{result.home.competitor}} - {{result.away.competitor}} {{result.home.pick}}:{{result.away.pick}}  ({{result.userScore}})
    </ion-item>
</ion-list>
  `,
})
export class LeaderPopoverPage {
  public results: any;

  constructor(private navParams: NavParams) {
      console.log('nav', );
      this.results = navParams.get('results');
  }
}