import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PageLoaderProvider {

  private switch: Subject<boolean> = new Subject();

  constructor() {
  }

  watch(): Observable<boolean> {
    return this.switch.asObservable();
  }

  isReady(isIt: boolean) {
    this.switch.next(isIt);
  }

}
