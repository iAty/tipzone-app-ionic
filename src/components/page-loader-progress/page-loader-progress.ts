import { Component, Input } from '@angular/core';

/**
 * Generated class for the PageLoaderProgressComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'page-loader-progress',
  templateUrl: 'page-loader-progress.html'
})
export class PageLoaderProgressComponent {

  @Input() isLoading: false;

  constructor() {

  }

}
