import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PasswordValidation } from '../../providers/password-validation';
import { LoadingController, NavController } from 'ionic-angular';
import { AuthenticationService } from '../../providers/user';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the RegisterFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'register-form',
  templateUrl: 'register-form.html'
})
export class RegisterFormComponent {
  public userDetails: FormGroup;
  errorSignUp: any;
  pleaseWaitText: string;
  constructor(private fb: FormBuilder, private loadingCtrl: LoadingController, private user: AuthenticationService, private navCtrl: NavController, private translate: TranslateService) {
    this.userDetails = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[a-zA-Z0-9_-]{6,}$')]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
      agreeTerms: [false, [Validators.requiredTrue]],
      newsletterSubscribe: [false],
    }, {
      validator: PasswordValidation.MatchPassword // your validation method
    });
    this.userDetails.valueChanges.subscribe( changed => this.errorSignUp = null);
    this.pleaseWaitText = this.translate.instant('PLEASE_WAIT');
  }

  signup() {

    let loading = this.loadingCtrl.create({
      content: this.pleaseWaitText
    });
    loading.present();
    // console.log('Valid', this.userDetails.valid);
    // console.log('Error', this.userDetails);
    if (this.userDetails.valid) {
      let details = this.userDetails.value;
      this.errorSignUp = null;

      this.user.register(details).then((user) => {
        loading.dismiss();
        this.navCtrl.push('ConfirmPage', { username: details.username });
      }).catch((err) => {
        loading.dismiss();
        this.errorSignUp = err;
      });
    } else {
      loading.dismiss();
    }
  }

}

