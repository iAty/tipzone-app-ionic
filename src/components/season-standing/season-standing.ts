import { Component, Input, OnInit } from '@angular/core';
import { DatabaseService } from '../../providers/db';
import { GroupByPipe } from 'ngx-pipes';

/**
 * Generated class for the SeasonStandingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'season-standing',
  templateUrl: 'season-standing.html',
  providers: [ GroupByPipe]
})
export class SeasonStandingComponent implements OnInit {
  @Input() seasonId: string
  tabella: any;

  constructor(
    private db: DatabaseService,
    private groupByPipe: GroupByPipe,
    ) {

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.db.getStandingBySeasonId(this.seasonId).then( data => {
      this.tabella = this.groupByPipe.transform(data, 'groupName');
      this.tabella = this.sortObject(this.tabella);
      // console.log('groupBy', this.tabella);
    })
  }

  sortObject(object): Array<any> {
    var sortedObj: any = [],
        keys: any = Object.keys(object);

    keys.sort(function(key1, key2){
        key1 = key1.toLowerCase(), key2 = key2.toLowerCase();
        if(key1 < key2) return -1;
        if(key1 > key2) return 1;
        return 0;
    });

    for(var index in keys){
        var key = keys[index];
        if(typeof object[key] == 'object' && !(object[key] instanceof Array)){
            sortedObj[key] = this.sortObject(object[key]);
        } else {
            sortedObj[key] = object[key];
        }
    }

    return sortedObj;
    }
}


