import { NgModule } from '@angular/core';
import { TagInputModule } from 'ngx-chips';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components/components.module';
import { MarkdownService, MarkedOptions, MarkdownModule } from 'ngx-markdown';
import { GroupsStore, GroupsStoreProvider } from '../stores/groups.store';
import { ResultsStore, ResultsStoreProvider } from '../stores/results.store';
import { UserStore, UserStoreProvider } from '../stores/user.store';
import { NewsStore, NewsStoreProvider } from '../stores/news.store';
import {NgPipesModule} from 'ngx-pipes';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NguCarouselModule } from '@ngu/carousel';
import { PasswordValidation } from '../providers/password-validation';






const TOP_SERVICES = [
  MarkdownService,
  MarkedOptions
];

const TOP_STORES = [
  GroupsStore,
  GroupsStoreProvider,
  ResultsStore,
  ResultsStoreProvider,
  UserStoreProvider,
  NewsStore,
  NewsStoreProvider
];

const SHARED_MODULES = [
  CommonModule,
  TagInputModule,
  FlexLayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ComponentsModule,
  NgPipesModule,
  NguCarouselModule,
];

@NgModule({
  imports: [
    ...SHARED_MODULES,
    MarkdownModule.forChild(),
    TranslateModule
  ],
  exports: [
    ...SHARED_MODULES,
    MarkdownModule,
    TranslateModule
  ],
  declarations: [],
  providers: [
    ...TOP_SERVICES,
    ...TOP_STORES,  
    TranslateService,
    PasswordValidation,
  ],
})
export class SharedModule { }
