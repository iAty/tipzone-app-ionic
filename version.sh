version_file=src/app/app.version.ts
> $version_file
now=`date +"%Y/%m/%d"`
echo "// This file was generated on $(date)
export const appVersion = '$1';
export const lastAppUpdate = '$now'" >> $version_file
git add $version_file
