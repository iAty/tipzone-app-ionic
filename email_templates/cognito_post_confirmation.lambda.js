var aws = require('aws-sdk');

const ses = new aws.SES({ region: 'eu-west-1', apiVersion: "2010-12-01" });

exports.handler = (event, context, callback) => {
    console.log(event);

    if (event.request.userAttributes.email) {
            sendEmail(event.request.userAttributes.email, event.userName, function(status) {

            // Return to Amazon Cognito
            callback(null, event);
        });
    } else {
        // Nothing to do, the user's email ID is unknown
        callback(null, event);
    }
};

function sendEmail(to, userName, completedCallback) {
  const params = {
  "Source": "TipZone <no-reply@tipzone.net>",
  "Template": "WelcomeEmail",
  "ConfigurationSetName": "TipzoneEmails",
  "Destination": {
    "ToAddresses": [to]
  },
  "TemplateData": "{ \"name\":\"" + userName + "\"}"
};

    var email = ses.sendTemplatedEmail(params, function(err, data){
        if (err) {
            console.log(err);
        } else {
            console.log("===EMAIL SENT===");
        }
        completedCallback('Email sent');
    });
    console.log("EMAIL CODE END");
};

