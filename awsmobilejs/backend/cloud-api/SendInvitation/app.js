/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

var express = require('express')
var bodyParser = require('body-parser')
var AWS = require('aws-sdk');
AWS.config.update({ region: process.env.MOBILE_HUB_PROJECT_REGION });
const dynamodb = new AWS.DynamoDB.DocumentClient();
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const mhprefix  = process.env.MOBILE_HUB_DYNAMIC_PREFIX;
const hasDynamicPrefix = true;
let tableName = "invitations";
// declare a new express app
var app = express()
const UNAUTH = 'UNAUTH';


AWS.config.update({
     accessKeyId: 'AKIAJSKSCH5MOE6WUYJQ',
    secretAccessKey: '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',
    region: 'eu-west-1'
  });
const ses = new AWS.SES({ apiVersion: "2010-12-01" });

if (hasDynamicPrefix) {
  tableName = mhprefix + '-' + tableName;
}


// Enable CORS for all methods
app.use(awsServerlessExpressMiddleware.eventContext({ deleteHeaders: false }), bodyParser.json(), function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});


AWS.config.update({ region: process.env.REGION })

// /**********************
//  * Example get method *
//  **********************/

// app.get('/send-invitation', function(req, res) {
//   // Add your code here
//   res.json({success: 'get call succeed!', url: req.url});
// });

// app.get('/send-invitation/*', function(req, res) {
//   // Add your code here
//   res.json({success: 'get call succeed!', url: req.url});
// });

/****************************
* Example post method *
****************************/

app.post('/send-invitation', function(req, res) {
  // Add your code here
   let people = [];
   let userId = null;
   let inviteId = null;
   let emailAddresses = [];

  if (req.apiGateway) {
    userId = req.apiGateway.event.requestContext.identity.cognitoIdentityId || UNAUTH;
  }
  if (req.body) {
    people = req.body['people'];
  }
  inviteId = req.body['inviteId'];
  let postBatchItemParams = {
    RequestItems: {}
  };

  postBatchItemParams.RequestItems[tableName] = [];
  
    for (let person of people) {
      
      emailAddresses.push(person.email);

      postBatchItemParams.RequestItems[tableName].push(
       { 
         PutRequest: {
            Item: {
              userEmail: person.email,
              groupName: req.body['groupName'],
              userName: req.body['userName'],
              seasonName: req.body['seasonName'],
              createdAt: new Date().toUTCString(),
              inviteId: inviteId,
              invitationFrom: userId
            }
         }
       })
    }

 
const TempInvitationData = JSON.stringify(
    {
      "userName": req.body['userName'],
      "invitedName": "felhasznalo",
      "groupName": req.body['groupName'],
      "seasonName": req.body['seasonName'],
      "invitationLink": "http://tipzone-hosting-mobilehub-1836564323.s3-website.eu-west-2.amazonaws.com/#/invitation/" +  req.body['userName'] + "/" + inviteId
    }
  );


const params = {
  "Source": "TipZone Jatek <no-reply@tipzone.net>",
  "Template": "GroupInvitationEmail",
  "ConfigurationSetName": "TipzoneEmails",
  "Destination": {
    "ToAddresses": emailAddresses
  },
  "TemplateData": TempInvitationData
}
  
  dynamodb.batchWrite(postBatchItemParams,(err, data) => {
    if(err) {
      res.json({error: 'Could not load items: ' + err.message});
    } else {
      ses.sendTemplatedEmail(params, (err, data) =>  {
          if (err) console.log(err, err.stack); // an error occurred
          else     console.log(data);           // successful response
        });
      if (data.Item) {
        res.json(data.Item);
      } else {
        res.json(data) ;
      }
    }
  });




  // res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/send-invitation/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example post method *
****************************/

// app.put('/send-invitation', function(req, res) {
//   // Add your code here
//   res.json({success: 'put call succeed!', url: req.url, body: req.body})
// });

// app.put('/send-invitation/*', function(req, res) {
//   // Add your code here
//   res.json({success: 'put call succeed!', url: req.url, body: req.body})
// });

// /****************************
// * Example delete method *
// ****************************/

// app.delete('/send-invitation', function(req, res) {
//   // Add your code here
//   res.json({success: 'delete call succeed!', url: req.url});
// });

// app.delete('/send-invitation/*', function(req, res) {
//   // Add your code here
//   res.json({success: 'delete call succeed!', url: req.url});
// });

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
