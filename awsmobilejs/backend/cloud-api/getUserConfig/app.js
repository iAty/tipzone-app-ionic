/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

var express = require('express');
var bodyParser = require('body-parser');
var AWS = require('aws-sdk');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
AWS.config.update({ region: process.env.MOBILE_HUB_PROJECT_REGION });
const dynamodb = new AWS.DynamoDB.DocumentClient();
const mhprefix  = process.env.MOBILE_HUB_DYNAMIC_PREFIX;
const hasDynamicPrefix = true;
let tableName = "user-custom-config";

if (hasDynamicPrefix) {
  tableName = mhprefix + '-' + tableName;
} 
const UNAUTH = 'UNAUTH';
// declare a new express app
var app = express()




// Enable CORS for all methods
app.use(awsServerlessExpressMiddleware.eventContext({ deleteHeaders: false }), bodyParser.json(), function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});


AWS.config.update({ region: process.env.REGION })

/**********************
 * Example get method *
 **********************/

app.get('/get-user-config', function(req, res) {
  // Add your code here
  let params = {};
  if (req.apiGateway) {
    params['userId'] = req.apiGateway.event.requestContext.identity.cognitoIdentityId || UNAUTH;
  }
  // params = req.body;

  let getItemParams = {
    TableName: tableName,
    Key: params
  }
  
  dynamodb.get(getItemParams,(err, data) => {
    if(err) {
      res.json({error: 'Could not load items: ' + err.message});
    } else {
      if (data.Item) {
        res.json(data.Item);
      } else {
        res.json(data) ;
      }
    }
  });
  // res.json({success: 'get call succeed!', url: req.url});
});

app.get('/get-user-config/*', function(req, res) {
  // Add your code here
  
  res.json({success: 'get call succeed!', url: req.url});
});

/****************************
* Example post method *
****************************/

app.post('/get-user-config', function(req, res) {
  // Add your code here
   let params = {};

  params = req.body;

  let postItemParams = {
    TableName: tableName,
    Item: params
  }
  
  dynamodb.put(postItemParams,(err, data) => {
    if(err) {
      res.json({error: 'Could not load items: ' + err.message});
    } else {
      if (data.Item) {
        res.json(data.Item);
      } else {
        res.json(data) ;
      }
    }
  });
  // res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/get-user-config/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example post method *
****************************/

app.put('/get-user-config', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

app.put('/get-user-config/*', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

/****************************
* Example delete method *
****************************/

app.delete('/get-user-config', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/get-user-config/*', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
